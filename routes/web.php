
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
Route::group(['domain' => 'localhost'],function() {
    // Route::group(['middleware' => ['web', 'language']], function () {
    //     Route::get('/', 'HomeController@getData')->name('/');

    //     Route::get('properties', 'PropertyController@getData')->name('properties');

    //     Route::get('properties/search', [
    //         'uses' => 'HomeController@searchHome',
    //         'as' =>'search.home',
    //     ]);
    //     Route::get('/search', [
    //         'uses' => 'SearchTestController@index',
    //         'as' => 'search.test'
    //     ]);
    //     Route::get('property/{listUrl}', [
    //         'uses' => 'PropertyController@showPropertyDetail',
    //         'as' => 'property.show',
    //     ]);

    //     Route::get('agents', 'AgentController@getData')->name('agents');

    //     Route::get('franchise', 'FranchiseController@getData')->name('franchise');

    //     Route::get('albums', 'GaleryController@getGallery')->name('albums');

    //     Route::get('gallery/{id}', [
    //         'uses' => 'GalleryDetailController@getGalleryDetail',
    //         'as' => 'gallery.detail',
    //     ]);

    //     Route::get('contact', 'ContactController@getData')->name('contact');

    //     Route::get('news', 'NewsController@getData')->name('news');

    //     Route::get('about', 'AboutController@getData')->name('about');

    //     Route::get('news/{id}', 'NewsController@newsDetail')->name('news.detail');

    // // Route::get('{domain}')->middleware(['domain' => '{domain}.localhost'])->name('domain.url');
    // });
});
/**
 * The sub-domain
 */



Route::group(['domain' => '{subdomain}.lifeincube.com'], function()
{
    Route::group(['middleware' => ['web', 'language']], function () {
        Route::get('/', 'HomeController@getData')->name('/');

        // Route::get('properties', 'PropertyController@getData')->name('properties');

        // Route::get('properties/search', [
        //     'uses' => 'HomeController@searchHome',
        //     'as' =>'search.home',
        // ]);
        // Route::get('/search', [
        //     'uses' => 'SearchTestController@index',
        //     'as' => 'search.test'
        // ]);
        // Route::get('property/{listUrl}', [
        //     'uses' => 'PropertyController@showPropertyDetail',
        //     'as' => 'property.show',
        // ]);

        // Route::get('agents', 'AgentController@getData')->name('agents');

        // Route::get('franchise', 'FranchiseController@getData')->name('franchise');

        // Route::get('albums', 'GaleryController@getGallery')->name('albums');

        // Route::get('gallery/{id}', [
        //     'uses' => 'GalleryDetailController@getGalleryDetail',
        //     'as' => 'gallery.detail',
        // ]);

        // Route::get('contact', 'ContactController@getData')->name('contact');

        // Route::get('news', 'NewsController@getData')->name('news');

        // Route::get('about', 'AboutController@getData')->name('about');

        // Route::get('news/{id}', 'NewsController@newsDetail')->name('news.detail');

   });
});
