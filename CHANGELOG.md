* Fix grid with flexbox css on property page `(DONE)`

* Fix bank slider on homepage `(DONE)`

* Fix language or global locale selector for all routes (see from Language middleware) `(DONE)`

* Fix form input validation with Vue.js `(DONE)`

* Fix social media share and add something social media missing on list `(DONE)`

* Fix price from blue to red `(DONE)`

* Fix social media working correctly on property page `(DONE)`

* Fix social media working correctly on gallery page `(DONE)`

* Fix i want to help from on property detail page `(DONE)`

* fix grid list social media button on property detail page `(DONE)`