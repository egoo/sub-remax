<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Psr7;
use App\Http\Helpers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PropertyController extends Controller
{
    protected $api;
    protected $uri;
    protected $client;
    protected $language;

    public function __construct(Api $api, Request $request)
    {
        $this->api = $api;
        $this->client = new GuzzleHttpClient(['base_uri' => $this->api->getBaseUri(),'verify' => false]);
        $this->uri = $this->api->getBaseUri();
        $this->language = $request->get('language');
    }

    public function index($domain)
    {
        return $domain;
    }

    public function getData()
    {
       try {

            $getClient = $this->client->get('listing' . '?language=' . Session::get('language') . '&fields=1&pageNumber=1&pageSize=100');
            $body = $getClient->getBody();
            $body = \GuzzleHttp\json_decode($body, false);
   
          
            $uri = $this->uri;

            $getClient = $this->client->get('Membership' . '?language=' . Session::get('language'));
            $body_member = $getClient->getBody();
            $body_member = \GuzzleHttp\json_decode($body_member, false);
            $uri = $this->uri;

           
            $getClient = $this->client->get('webmenu?language=' . Session::get('language') . '&filter[wbmnTo]=\'%c%\'' . '&sort=%2BwbmnOrder');
            $getLanguage = $this->client->get('language');

            $header = $getClient->getBody()->getContents();
            $header = \GuzzleHttp\json_decode($header, false);

            $language = $getLanguage->getBody()->getContents();
            $language = \GuzzleHttp\json_decode($language, false);   


            if ($getClient->getStatusCode() == 200 && $getLanguage->getStatusCode() == 200) {
                return view('search')
                    ->with(['body' => $body])
                    ->with(['header' => $header])
                    ->with(['language' => $language])
                    ->with(['body_member' => $body_member])
                    ->with(['uri' => $uri]);
            } else {
                return redirect()->back()
                    ->with('error', 'something is error with API');
            }
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function showPropertyDetail($listUri)
    {
        try {

            $getClient = $this->client->get('listing/'.$listUri.'?language='.Session::get('language'));
            // var_dump($listUri);
            $body = $getClient->getBody();
            $body = \GuzzleHttp\json_decode($body, false);
            $uri = $this->uri;

            // $test = $this->client->get('https://www.remax.co.id/prodigy/papi/Listing/crud/85/links/ListingFile/115');
            // $test1 = $getClient->getBody();    
            // $test1 = json_de($test);

            // get Office Id
            $officeId = $body->linked->listOfficeId->frofId;
            $getOffice = $this->client->get('listing?filter[listOfficeId]='.$officeId.'&language='.Session::get('language').'&fields=1&pageNumber=1&pageSize=20');
            $office = $getOffice->getBody();
            $office = \GuzzleHttp\json_decode($office, false);
            // end office id

            // get membership
            $membershipId = $body->linked->listMmbsId->mmbsId;
            $getMembership = $this->client->get('Membership/'.$membershipId);
            $membership = $getMembership->getBody();
            $membership = \GuzzleHttp\json_decode($membership, false);

            // $encodedJson = json_encode((array) $body->data->links->listFacility);
            // $decodeJson = json_decode($encodedJson, true);


            // dd($body->data->links->listFacility, $body->linked->listFacility);            

             
             // foreach ($body->linked->listFacility as $data) {
             //    foreach ($body->data->links->listFacility as $value) {   
             //       dd($value->{$data->id}, $data->fctlName);
             //    }
                
             // }

            // foreach (array_combine($links, $linked) as $links1 => $linked1) {
            //          dd($links1, $linked1);
            // }

            //  $combine = array_map(null, $links, $linked);
            // dd($combine);


            // dd($decodeJson);
            
            //    // foreach ($body->data->links->listFacility as $key => $value)
            //    //          {
            //    //              if (is_array($value) || is_object($value))
            //    //              {
            //    //                  $test = $value;
            //    //                       dd($test);
            //    //              }
            //    //              else
            //    //              {
            //    //                  // DO SOMETHING
            //    //              }
                       
            //    //          }
                    
        
            // dd($body->data->links->listFacility);


            if ($getClient->getStatusCode() === 200) {

                return view('property_page')
                    ->with(['body' => $body])
                    ->with(['uri' => $uri])
                
                    ->with(['membership' => $membership])
                    ->with(['office' => $office])
                    ->with(['domain' => "http://".strtolower($body->linked->listOfficeId->frofOfficeName)]);
            } else {
                return redirect()
                    ->back()
                    ->with('error', 'something error with API');
            }
        } catch (RequestException $e) {
        
             
                $exc = $e->getResponse()->getBody();
                $exc = json_decode($exc);
        if($exc->status->error->code === 404)
            return view('errors.404'); 
        }
    }

    public function postInquiry(Request $request)
    {
    }
}
