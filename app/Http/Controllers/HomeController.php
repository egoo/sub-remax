<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Psr7;
use App\Http\Helpers\Api;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    protected $api;
    protected $client;
    protected $uri;

    public function __construct(Api $api, Request $request)
    {
        $this->api = $api;
        $this->client = new GuzzleHttpClient(['base_uri' => $this->api->getBaseUri(),'verify' => false]);
        $this->uri = $this->api->getBaseUri();
    }

    public function getData()
    {
        try {
            $getClient = $this->client->get('franchise');
            $body = $getClient->getBody();
            $body = \GuzzleHttp\json_decode($body, false);
            $uri = $this->uri;


            $getLanguage = $this->client->get('ListingCategory?language='. Session::get('language'));
            $getClient = $this->client->get('bank');

            $data = $getClient->getBody();
            $langApi = $getLanguage->getBody();

            $lg = \GuzzleHttp\json_decode($langApi, false);

            $body = \GuzzleHttp\json_decode($data, false);

            $uri = $this->uri;

            if ($getClient->getStatusCode() == 200 && $getLanguage->getStatusCode() == 200) {
                return view('indexoffice', compact('body', 'uri', 'lg'));
            } else {
                return redirect()
                    ->back()
                    ->with('error', 'something is error with API');
            }
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function searchHome(Request $request)
    {
        try {
            $getClient = $this->client->get('listing' . '?language=' . Session::get('language') . '&fields=1&pageNumber=1&pageSize=100');
            $body = $getClient->getBody();
            $body = \GuzzleHttp\json_decode($body, false);
            $uri = $this->uri;

            $getClient = $this->client->get('Membership' . '?language=' . Session::get('language'));
            $body_member = $getClient->getBody();
            $body_member = \GuzzleHttp\json_decode($body_member, false);
            $uri = $this->uri;

            $getClient = $this->client->get('webmenu?language=' . Session::get('language') . '&filter[wbmnTo]=\'%c%\'' . '&sort=%2BwbmnOrder');
            $getLanguage = $this->client->get('language');

            $header = $getClient->getBody()->getContents();
            $header = \GuzzleHttp\json_decode($header, false);

            $language = $getLanguage->getBody()->getContents();
            $language = \GuzzleHttp\json_decode($language, false); 

            if ($getClient->getStatusCode() == 200 && $getLanguage->getStatusCode() == 200) {
                return view('search')
                    ->with(['body' => $body])
                    ->with(['header' => $header])
                    ->with(['language' => $language])
                    ->with(['body_member' => $body_member])
                    ->with(['uri' => $uri]);
            } else {
                return redirect()->back()
                    ->with('error', 'something is error with API');
            }
        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }
}
