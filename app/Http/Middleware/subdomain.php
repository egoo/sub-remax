<?php

namespace App\Http\Middleware;

use Closure;
use IlluminateSupportFacadesApp;
use App;
class subdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
          $subdomain = $url_array[0];

          if ($subdomain) {
              App::setLocale($subdomain);
          }
          
          return $next($request);
    }
    
}
