<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Language
{
    protected $request;

    public function handle($request, Closure $next)
    {
        $language = $request->get('language');
        
        if ($this->isSessionValid()) {
            $this->forceSetLocaleSession($request);
        }

        if ($language) {
            $this->storeSession($language);
        }
        
        return $next($request);
    }

    /**
     * @return bool
     */
    protected function isSessionValid()
    {
        return Session::has('language');
    }

    /**
     * @param $request
     * @throws \Exception
     */
    protected function forceSetLocaleSession($request)
    {
        if (!$request instanceof Request) {
            throw new \Exception('request variable does not valid object instanceof with Request class');
        }

        $request->setLocale($this->getCurrentSessionLanguage());
    }

    /**
     * @return mixed
     */
    protected function getCurrentSessionLanguage()
    {
        return Session::get('language');
    }

    /**
     * @param $language
     */
    protected function storeSession($language)
    {
        Session::put('language', $language);
    }
}
