<?php

namespace App\Http\Helpers;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Psr7;

class Api
{
    private $baseUri = "https://www.remax.co.id/prodigy/papi/";
    protected $api;

    public function __construct()
    {
        $this->api = new GuzzleHttpClient(['base_uri' => $this->baseUri,'verify'=> false]);
    }

    public function getBaseUri()
    {
        return $this->baseUri;
    }
}
