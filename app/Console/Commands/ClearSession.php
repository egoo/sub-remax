<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:session';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all session storage based on filesystem';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $realpath = dirname(dirname(dirname(__DIR__)));

    }
}
