    <!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel='stylesheet' type='text/css' href='http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css'/>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/customsas.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />
    <title>Agents</title>
    <style>
.form-group{
            margin-bottom: 0px;
        }
/*.inner-addon {
  position: relative;
}


.inner-addon .glyphicon {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}


.left-addon .glyphicon  { left:  0px;}
.right-addon .glyphicon { right: 0px;}


.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }*/
    </style>
</head>

<body class="page" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper" >
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->
    <!-- Page Content -->

  
    {{--<div id="page-content" class="alignCenter">--}}

    <div class="wide-1" id="form-inquiry">

        {{--<div id="page-content" class="contact-us">--}}

        <?php $uri = 'http://genius.intelligence.id/papi/'; ?>

        @include('layout.modal_inquiry_agent')




        @foreach($body->data as $content)

            @foreach($body->linked->wbinFileId as $linked)
                @if($content->links->wbinFileId == $linked->id)
                    <div class="parallax"
                         style="background-image: url({{ $uri.$linked->filePreview }})">
                        <div class="caption">
                            <span class="border">{{ $content->wbilTitle }}</span>
                        </div>

                    </div>
                @endif   

            @endforeach
           
            <div class="wrapper-main-agent">

                <?php echo $content->wbilContent; ?>
            </div>
          
        @endforeach
        
    </div>
</div>
  

<div id="footer">@include('layout.footer')</div>

</div>
<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<!--[if gt IE 8]-->

<script type="text/javascript" src="{{ asset('assets/js/vee-validate/dist/vee-validate.min.js') }}"></script>

<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/vue-filter.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vuejs-paginator/2.0.0/vuejs-paginator.js"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/lodash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/ie.js') }}"></script>
<!--[endif]-->

<script>
    Vue.config.devtools = true;
    Vue.use(VeeValidate);
    new Vue({
        el: '#form-inquiry',
        data: {
            error: false,
            flag: false,
            flgEmail: false,
            email: '',
            name: '',
            phone: '',
            inquiry: {
                name: '',
                phone: '',
                email: '',
                message: ''
            },
            search: '',
            namemmbs: [] ,
            loading: false,
        },  

        created: function() {
            this.fetchAgents();
            
        },


        methods: {
            onHandle: function () {
                this.flag = !this.flag;
            },
            validateBeforeSubmit() {
                this.$validator.validateAll()
                    .then(function() {
                        alert('success send message');
                        location.reload();
                    })
                    .catch(function() {
                        alert('form must be fulfilled and cannot be empty');
                    })
            },
            postInquiry: function () {
                var vm = this;
                axios.post('https://www.remax.co.id/prodigy/papi/inquiryagent/crud',
                            {
                            "inagCustomerName": vm.inquiry.name,
                            "inagCustomerEmail": vm.inquiry.email,
                            "inagCustomerPhone": vm.inquiry.phone,
                            "inagCustomerBody": vm.inquiry.message,
                        })
                        .then(function (response) {
                            
                            console.log(response)
                            alert('success send');
                        })
                        .catch(function (error) {
                            vm.loading = false;
                            console.log(error)
                            alert('gagal send');
                        })

            },

            fetchAgents: function(){
                var vm = this;
                if( vm.namemmbs == ''){
                    vm.namemmbs = ''


                }
                axios.get('https://www.remax.co.id/prodigy/papi/membership?filter[mmbsFirstName]=%27'+vm.namemmbs +'%27')
                .then(function(response){
                    console.log(response)
                        if(response.data.data != null){
                            vm.error = false;
                             vm.search = response.data.data;   
                        }else{
                            vm.error = false;
                             
                        }
                        
                       

                })
                .catch(function(error){
                    console.log(error)

                });

            }



        }
    })
</script>

</body>
</html>