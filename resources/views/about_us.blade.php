<!DOCTYPE html>

<html lang="en-US">
<style>
    hr {
        border: 0;
        clear: both;
        display: block;
        width: 96%;
        background-color: grey;
        height: 1px;
        margin-bottom: 5px;
        margin-top: 5px;
    }
     @media only screen and (max-width: 768px){
        .para {
                min-height: 267px;
                width: 100%;
                height: auto;
            }
    } 
</style>

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />
    <title>About</title>
</head>

<body class="page" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper prop" id="wrapper-about" >
    <!-- Start Header -->
    @include('layout.header')
    <div class="wide-1">

        <?php $uri = 'http://genius.intelligence.id/papi/'?>

        @foreach($body->data as $content)

            @foreach($body->linked->wbabFileId as $linked)
                @if($content->links->wbabFileId == $linked->id)
                <div class="parallax para hidden-xs hidden-sm"
                     style="background-image: url({{ $uri.$linked->filePreview }})">
                    <div class="caption">
                        <span class="border">{{ $content->wbalTitle }}</span>
                    </div>
                </div>
                <div class=" hidden-lg"> 
                 <div class="parallax para ">
                     <img src="{{ $uri.$linked->filePreview }}" class="img-responsive">
                    <div class="caption">
                        <span class="border">{{ $content->wbalTitle }}</span>
                    </div>
                </div>
                </div>
                @endif
            @endforeach
            <div class="wrapper-main-agent">
                <div class="content-main-agent">
                    <div class="container">
                        {!! $content->wbalContent !!}
                    </div>
                </div>
            </div>
        @endforeach
   
    </div>
</div>

<div id="footer">@include('layout.footer')</div>

<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/scrollability.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/stellar.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/scrollability.min.js')}}"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<!--[if gt IE 8]-->
<script type="text/javascript" src="{{ asset('assets/js/ie.js') }}"></script>


<!--[endif]-->
<script type="text/javascript">
    $(".para").(function () {
        $(this).lazyload({
            effect: "fadeIn",
            thumbnail: true,
            animateThumb: true,
            showThumbByDefault: true
        });
    });

    setTimeout(function(){
        listedHover();

        markerHover();

    }, 3000);
</script>
<script type="text/javascript">
    $(function(){
            $('#page').stellar({
                horizontalScrolling: false,
                scrollProperty: 'transform',
                positionProperty: 'transform',
                verticalOffset: -100
            });
            
            //Prevent text selection
            $(document).mousedown(function(e){
                e.preventDefault();
            });
        });
</script>
</body>


</html>