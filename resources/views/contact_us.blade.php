<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/customsas.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />

    <title>Contact Us</title>


    <style>
        @media screen and (min-width: 240px) and (max-width: 768px) {
            .contact-form {
                margin-top: 10px;
                position: relative;
            }
        }
    </style>
</head>


<body class="page" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <div id="page-content">
        <div class="section-contact-us" style="background-image: url({{ asset('images/backgroundPage/contact-us-alt1.jpg') }});background-size:cover;">
            <div class="container">
                <div class="row">
                    <div id="map2" class="map contact-map"></div>
                    <div class="contact-form">
                        <h3 style="color: white;"><i class="fa fa-envelope"></i>&nbsp;&nbsp;SEND US A MESSAGE </h3>
                        <hr>
                        <form id="contact-form" class="form-submit" @submit.prevent="validateBeforeSubmit">
                            <div class="col-sm-12">
                                <div class="input-group">

                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('name')}">
                                <label>Name</label>
                                    <input v-validate="'required|alpha'" :class="{'input': true, 'is-danger': errors.has('name')}" style="text-transform: capitalize;" v-model="inquiry.name" name="name" class="form-control" type="text"
                                       placeholder="name">
                                    <span v-show="errors.has('name')" class="text-danger">@{{ errors.first('name') }}></span>
                                </div>

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-group">

                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('email')}">
                                <label>Email</label>
                                <input v-validate="'required|email'" :class="{'input': true, 'is-danger': errors.has('email')}" style="text-transform: capitalize;" v-model="inquiry.email" name="email" class="form-control" type="text"
                                       placeholder="email">
                                    <span v-show="errors.has('email')" class="text-danger">@{{ errors.first('email') }}></span>
                                </div>

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-group">

                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('phone')}">
                                <label>Phone</label>
                                <input v-validate="'required|numeric'" :class="{'input': true, 'is-danger': errors.has('phone')}" style="text-transform: capitalize;" v-model="inquiry.phone" name="phone" class="form-control" type="text"
                                       placeholder="phone">
                                    <span v-show="errors.has('phone')" class="text-danger">@{{ errors.first('phone') }}></span>
                                </div>

                                </div>
                            </div>

                         <!--    <div class="col-sm-12">
                                <div class="input-group">
                                    <label>Subject:</label>
                                    <select class="form-control" name="" id="" >
                                        <option value="" disabled="" selected="">Please Select subject</option>
                                        <option  v-for="cont in contact" 
                                                                        :value="cont.id">@{{ cont.coulSubject}}</option>
                                    </select>
                                </div>
                            </div>
 -->
                            <div class="col-sm-12">
                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('message')}">
                                    <label>Your Message:</label>
                                    <textarea id="text-area-contact" v-model="inquiry.message" rows="4" cols="20" style="text-transform: capitalize;" class="form-control" placeholder="Send Message"></textarea>
                                    <span v-show="errors.has('message')" class="text-danger">@{{ errors.first('message')}}</span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                {{-- <span class="ffs-bs input-group"> --}}
                                <div class="input-group">
                                    <button type="submit" class="form-control btn btn-large btn-danger center-block" @click="postInquiry">Send Message
                                    </button>
                                </div>
                                {{-- </span> --}}
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end Page Content -->

        <!-- Start Footer -->
        <div id="footer">@include('layout.footer')</div>
        <!-- End Footer -->
    </div>
</div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBt6Pwo89NTFbXWEtrcEsL14M6af-gSjfQ&libraries=places"></script>
    <script src="{{ asset ('js/vue.min.js') }}"></script>
    <script src="{{ asset ('js/axios.min.js') }}"></script>
    <script src="{{ asset ('assets/js/vee-validate/dist/vee-validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/markerwithlabel_packed.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom-map.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>

    <!--[if gt IE 8]-->
    <script type="text/javascript" src="{{ asset('assets/js/ie.js')}}"></script>
    <![endif]-->

    <script>
        //Google map for contact us page
        $(document).ready(function () {
            function initialize() {
                var latlng = {lat: -6.225673, lng: 106.808481};
                var mapOptions = {
                    center: latlng,
                    zoom: 14
                };
                var map = new google.maps.Map(document.getElementById('map2'),
                        mapOptions);
                map.set('styles', [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]);
 var icon = {
                                                url: "../images/new-map-marker.png", // url
                                                scaledSize: new google.maps.Size(40, 40), // scaled size
                                                origin: new google.maps.Point(0,0), // origin
                                                anchor: new google.maps.Point(0, 0) // anchor
                                            };

                                            marker = new MarkerWithLabel({
                                                position: latlng,
                                                map: map,

                                                // labelContent: '<div class="marker-loaded" data-target="marker'+vm.property[index].id+'"><div class="map-marker"><img src="../images/new-map-marker.png" alt="" height="27" width="27"/></div></div>',
                                                // labelAnchor: new google.maps.Point(10, 0),
                                                // labelClass: "marker-style",
                                                icon: icon

                                            });
                // var marker = new MarkerWithLabel({
                //     position: latlng,
                //     map: map,
                //     labelContent: '<div class="marker-loaded"><div class="map-marker"><img src="{{ asset('images/map_marker.png') }}" alt=""></div></div>',
                //     labelClass: "marker-style"
                // });

                var contentString = '<div id="mapinfo">' +

                '<h4 class="firstHeading">{{$body->data->compName}}</h4>' +

                        '<h6></h6>' +
                        '<div><i class="fa fa-phone"></i><a href="tel:+48 192 28383746">Phone : {{$body->data->compPhone1}} </a></div>' +
                        '<div><i class="fa fa-mobile"></i><a href="tel:+48 192 28383746">Fax : {{ $body->data->compFax1 }}}</a></div>' +
                        '<p id="at">@</p>' +
                        '<div class="contactblock"><a href="#">{{ $body->data->compEmail }}</a></div>' +

                    '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        });
    </script>
    <script>
        Vue.use(VeeValidate);
        new Vue({
            el: '#contact-form',
            data: {
                flag: false,
                flgEmail: false,
                email: '',
                name: '',
                phone: '',
                inquiry: {
                    name: '',
                    phone: '',
                    status: '',
                    email: '',
                    message: ''
                },
                contact: [],
                loading: false,
            },
            created: function(){
                this.fecthContact();


            },
            methods: {

                fecthContact: function(){
                    var vm = this;
                    axios.get('https://www.remax.co.id/prodigy/papi/contactus')
                    .then(function(response){
                        console.log(response)    
                        if(response.data.data == null){
                            vm.contact =[];
                        }else{
                          vm.contact = response.data.data;
                        }

                            
                    })
                    .catch(function(error){
                        console.log(error)
                    });
                },


                validateBeforeSubmit() {
                this.$validator.validateAll()
                    .then(function() {
                        alert('success send message');
                        location.reload();
                    })
                    .catch(function() {
                        alert('form must be fulfilled and cannot be empty');
                    })
                },

                postInquiry: function () {
                var vm = this;
                axios.post('https://www.remax.co.id/prodigy/papi/inquirycontactus/crud',
                        {
                            "ictsCustomerName": vm.inquiry.name,
                            "ictsCustomerEmail": vm.inquiry.email,
                            "ictsCustomerPhone": vm.inquiry.phone,
                            "ictsCustomerBody": vm.inquiry.message,
                            "links": {
                                "ictsMscuId": 1

                            }
                        })
                        .then(function (response) {
                            vm.loading = true;
                            console.log(response)
                        })
                        .catch(function (error) {
                            vm.loading = false;
                            console.log(error)
                        })

            },  
              


            }
        })
     </script>
</body>
</html>