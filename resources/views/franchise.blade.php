<!DOCTYPE html>



<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />
    <title>Franchise</title>
</head>
<style >
      @media only screen and (max-width: 768px){
        .para {
                min-height: 200px;
                width: 100%
                height: auto;
            }
    } 
</style>
<body class="page" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->
<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->
    <!-- Page Content -->
    {{--<div id="page-content" class="alignCenter">--}}
    <div class="wide-1">
        {{--<div id="page-content" class="contact-us">--}}

        <?php $uri='http://genius.intelligence.id/papi/';?>

        @include('layout.modal_franchise')

        @foreach($body->data as $content)

            @foreach($body->linked->wbfrFileId as $linked)

                @if($content->links->wbfrFileId == $linked->id)
                    <div class="parallax para hidden-sm hidden-xs"
                         style="background-image: url({{ $uri.$linked->filePreview }})">
                        <div class="caption">
                            <span class="border">{{ $content->wbflTitle }}</span>
                        </div>
                    </div>
                <div class=" hidden-lg"> 
                 <div class="parallax para ">
                     <img src="{{ $uri.$linked->filePreview }}" class="img-responsive">
                    <div class="caption">
                        <span class="border">{{ $content->wbflTitle }}</span>
                    </div>
                </div>
                </div>
                @endif

            @endforeach

                <div class="wrapper-main-agent">
                    <div class="content-main-agent">
                        <?php echo $content->wbflDescription;?>
                    </div>
                </div>

        @endforeach

    </div>
</div>

<div id="footer">@include('layout.footer')</div>

<script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/vee-validate/dist/vee-validate.min.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<!--[if gt IE 8]-->
<script type="text/javascript" src="{{ asset('assets/js/ie.js') }}"></script>
<!--[endif]-->
<script>
 Vue.config.devtools = true;
    Vue.use(VeeValidate);
    new Vue({
        el: '#form-inquiry',
        data: {
            flag: false,
            flgEmail: false,
            email: '',
            name: '',
            phone: '',
            inquiry: {
                name: '',
                phone: '',
                email: '',
                message: ''
            },
            loading: false,
        },

        methods: {
            onHandle: function () {
                this.flag = !this.flag;
            },
            validateBeforeSubmit() {
                this.$validator.validateAll()
                    .then(function() {
                        alert('success send');
                        location.reload();
                    })
                    .catch(function() {
                        alert('form must be fulfilled and cannot be empty');
                    })
            },
            postInquiry: function () {
                var vm = this;
                axios.post('https://www.remax.co.id/prodigy/papi/inquiryfranchise/crud',
                        {
                            "infsCustomerName": vm.inquiry.name,
                            "infsCustomerEmail": vm.inquiry.email,
                            "infsCustomerPhone": vm.inquiry.phone,
                            "infsCustomerBody": vm.inquiry.message,

                        })
                        .then(function (response) {
                            vm.loading = true;
                            console.log(response)
                        })
                        .catch(function (error) {
                            vm.loading = false;
                            console.log(error)
                        })

            }

        }
    })
</script>
</body>
</html>