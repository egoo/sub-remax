
    @extends('layout/layout')
    
    @section('header')
    @endsection
    @section('content')

                <template id="pagination-template">
                  <ul class="pagination pull-right">
                    <li class="page-item"><a class="page-link" href="#" v-if="hasPrev()" @click="changePage(prevPage)">Previous</a></li>
                      <li  v-for="page in pages" :class="'page-item' + { 'active': current == page }"> 
                        <a class="page-link"  href="#" @click="changePage(page)">@{{ page }}</a>
                      </li>
                    <li class="page-item"><a class="page-link" href="#" v-if="hasNext()" @click="changePage(nextPage)">Next</a></li>
                  </ul>
                </template>

  <div id="page-content">
      <div class="main-wrapper">
        <div class="main">
          <div class="main-inner">
            <!-- /.page-title -->
            <div class="container">
              <nav class="breadcrumb">
                <a class="breadcrumb-item" href="#">Home</a>
                <a class="breadcrumb-item" href="#">Library</a>
                <a class="breadcrumb-item" href="#">Data</a>
                <span class="breadcrumb-item active">Bootstrap</span>
              </nav>
              <div class="row">
                <div class="listing-detail col-md-8 col-lg-9">
                  <div class="listing-user">
                    <img src="{{ asset('assets/img/remax_default.jpg')}}" class="baner-image">
                  </div>
                  <!-- /.listing-user -->
                  <div class="row">
                    <div class="col-lg-5">
                      <div class="overview">
                        <h3 class="page-title-small">Information</h3>
                        <ul v-for="office in frachise">

                          <li><strong>Name</strong><span>John Doe</span></li>
                          <li><strong>Phone</strong><span>+0-123-456-789</span></li>
                          <li><strong>E-mail</strong><span><a href="#">sample@example.com</a></span></li>
                          <li><strong>Website</strong><span><a href="#">www.example.com</a></span></li>
                          <li><strong>Real Estate</strong><span>Estate Agency, Inc.</span></li>
                       
                        </ul>
                      </div>
                      <!-- /.overview -->
                    </div>
                    <!-- col-* -->
                    <div class="col-lg-7">
                      <h3 class="page-title-small">About Remax Office</h3>
                      <p class="mb30">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna. Proin ac massa at arcu semper consectetur ut eget est. Praesent dignissim consectetur mollis. Mauris tristique interdum luctus. Fusce sed arcu pretium, elementum lorem sed, pellentesque ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna. 
                      </p>
                    </div>
                    <!-- /.col-* -->
                  </div>
                  <!-- /.row -->
                  <!-- /.box -->
                  <h3 class="page-title-small">Agent Assigned Listings</h3>
               
                
                  <div class="row">
                  
                      <div class="col-md-6 col-lg-4" v-for="prop in property">
                        <div class="listing-box">
                          <div class="listing-box-inner">
                            <span v-if="prop.links.listListingCategoryId == 1" class="listing-box-featured">For Sale</span>
                            <span v-if="prop.links.listListingCategoryId == 2" class="listing-box-featured-blue">For Rent</span>
                            <a href="listings-detail.html" class="listing-box-image">
                            <span v-if="prop.links.listFile">
                              <span class="listing-box-image-content" :style="{ backgroundImage: 'url(' + imageUri +  fil.filePreview + ')'}" v-for="fil in file" v-if="fil.id == prop.links.listFile[0]"></span><!-- /.listing-box-image-content -->
                            </span>
                            <span v-else class="listing-box-image-content" style="background-image: url({{ asset('images/Not_available.jpg') }})"></span>

                              <span class="listing-box-category tag">@{{ formatRp(prop.listListingPrice) }}</span>
                              <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                            </a><!-- /.listing-box-image -->
                            <div class="listing-box-content">
                              <h2><a href="listings-detail.html">@{{ prop.listTitle }}</a></h2>
                              <h3><i class="fa fa-map-marker"></i> @{{ prop.listStreetName }}</h3>
                              <div class="actions">
                                <div class="actions-button">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </div>
                                <!-- /.actions-button -->
                                <!-- <ul class="actions-list">
                                  <li><a href="#">Add to compare</a></li>
                                  <li><a href="#">Add to favorites</a></li>
                                  <li><a href="#">Report listing</a></li>
                                </ul> -->
                                <!-- /.actions-list -->
                              </div>
                              <!-- /.actions -->
                            </div>  
                            <!-- /.listing-box-content -->
                             <div class="listing-box-content">
                              <h3> @{{ prop.listDescription }}</h3>
                              <!-- /.actions -->
                            </div> 
                            <!-- /.listing-box-attributes -->
                            <div class="listing-box-attributes-icons">
                              <ul>
                                <li><i class="fa fa-arrows"></i> <span>@{{ prop.listLandSize }} m2</span></li>
                                <li><i class="fa fa-shower"></i> <span>@{{ prop.listBathroom }}</span></li>
                                <li><i class="fa fa-home"></i> <span>@{{ prop.listBuildingSize }} m2</span>
                                <li><i class="fa fa-bed"></i> <span>@{{ prop.listBedroom }}</span>
                                </li>
                              </ul>
                            </div>
                            <!-- /.listing-box-attributes -->
                          </div>
                          <!-- /.listing-box-inner -->
                        </div>
                        <!-- /.listing-box -->
                      </div>
                    <!-- /.col-* -->
                  </div>
             
                  <!-- /.row -->
                  <pagination :current="currentPage"
                              :perPage="perPage"
                              :total="totalProperty"
                              @page-changed="fetchProperty">
                   </pagination>
                </div>
                <div class="col-md-4 col-lg-3">
                  <div class="sidebar">
                    <!-- <div class="widget">
                      <h3 class="widgettitle">Search Site</h3>
                      <div class="search">
                        <form method="get" action="?">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" placeholder="Search the site" class="form-control">
                          </div> -->
                          <!-- /.form-group -->
                        <!-- </form> -->
                      <!-- </div> -->
                      <!-- /.search -->
                    <!-- </div> -->
                    <!-- /.widget -->
                    <!-- <div class="widget">
                      <h2 class="widgettitle">Public Documents</h2>
                      <div class="documents">
                        <a class="document" href="#">
                          <i class="fa fa-file-image-o"></i> <span>Image Gallery</span>
                        </a>
                        <a class="document" href="#">
                          <i class="fa fa-file-excel-o"></i> <span>Detailed Pricing</span>
                        </a>
                        <a class="document" href="#">
                          <i class="fa fa-file-pdf-o"></i> <span>Download Floor Plans</span>
                        </a>
                      </div> -->
                      <!-- /.documents -->
                    <!-- </div> -->
                    <div class="widget">
                      <h3 class="widgettitle">Top Agents</h3>
                      <div v-for="agent in topagent">
                        <div class="listing-row-medium">
                          <div class="listing-row-medium-inner">
                            <!-- <a href="listings-detail.html" class="listing-row-medium-image" style="background-image: url(assets/img/tmp/listing-12.jpg);">
                            </a>
                            <div class="listing-row-mediumedium-content"> -->
                              <!-- <a class="listing-row-medium-price" href="listings-detail.html">$533.00</a>
                              <a class="listing-row-medium-category tag" href="listings-detail.html"></a>
                              <h4 class="listing-row-medium-title"><a href="listings-detail.html">@{{ agent.mmbsFirstName }}</a></h4>
                              <div class="listing-row-medium-address">
                                
                              </div> -->
                              <div class="listing-user">

                                <a v-if="agent.links.mmbsFile != null" href="#" class="listing-row-medium-image"  :style="{ backgroundImage: 'url(' + imageUri + agent.mmbsFile + ')'}"></a>

                                <a v-else href="#" class="listing-row-medium-image" style="background-image: url('{{ asset('images/default.jpg') }}');"></a>

                                <div class="listing-user-title">
                                <a class="listing-row-medium-category tag" href="listings-detail.html">Agent</a>
                                  <h2>@{{ agent.mmbsFirstName }}</h2><br>
                                   
                                  <!-- <h3>Senior Directory Manager</h3> -->
                                </div>
                                <!-- /.listing-user-title -->
                              </div>
                              <!-- /.listing-row-medium-address -->
                            </div>
                            <!-- /.listing-row-medium-content -->
                          </div>
                          <!-- /.listing-row-medium-inner -->
                        </div>
                        <!-- /.listings-row-medium -->
                      </div>
                    </div>
                    <!-- /.widget -->
                    <!-- <div class="widget">
                      <h2 class="widgettitle">Contact Information</h2>
                      <table class="contact">
                        <tbody>
                          <tr>
                            <th>Address:</th>
                            <td>2300 Main Ave.<br>
                              Lost Angeles, CA 23123<br>
                              United States<br>
                            </td>
                          </tr>
                          <tr>
                            <th>Phone:</th>
                            <td>+0-123-456-789</td>
                          </tr>
                          <tr>
                            <th>E-mail:</th>
                            <td><a href="mailto:info@example.com">info@example.com</a></td>
                          </tr>
                          <tr>
                            <th>Skype:</th>
                            <td>your.company</td>
                          </tr>
                        </tbody>
                      </table>
                    </div> -->
                    <!-- /.widget -->
                  </div>
                  <!-- /.sidebar -->
                </div>
                <div class="listing-detail col-md-8 col-lg-9">
                 
                  <!-- /.listing-user -->
                
                  <!-- /.row -->
                  <div class="box">
                    <div class="box-inner">
                      <div class="box-title">
                        <h2>Contact Information</h2>
                        <p>
                          In dictum fringilla sem vitae condimentum. Maecenas venenatis odio et eros venenatis, vel bibendum mauris elementum. Maecenas id lobortis tortor. Nam condimentum pulvinar odio eget tincidunt. Curabitur in mattis enim.         
                        </p>
                      </div>
                      <!-- /.box-title -->
                      <form method="post" action="?">
                        <div class="row">
                          <div class="col-sm-5">
                            <div class="form-group">
                              <label>Name</label>
                              <input type="text" class="form-control">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                              <label>E-mail</label>
                              <input type="text" class="form-control">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                              <label>Subject</label>
                              <input type="text" class="form-control">
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col-* -->
                          <div class="col-sm-7">
                            <div class="form-group">
                              <label>Message</label>
                              <textarea class="form-control" rows="10" placeholder="Please keep your message as simple as possible."></textarea>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col-* -->
                          <div class="col-sm-12">
                            <div class="form-group-btn">
                              <button class="btn btn-primary pull-right">Send Message</button>
                            </div>
                            <!-- /.form-group-btn -->
                          </div>
                          <!-- /.col-* -->
                        </div>
                        <!-- /.row -->
                      </form>
                    </div>
                    <!-- /.box-inner -->
                  </div>
                  <!-- /.box -->
              
                </div>
                <!-- /.col-* -->
                
              </div>
              <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
          </div>
          <!-- /.main-inner -->
        </div>
        <!-- /.main -->
      </div>
      </div>
      @endsection
      <!-- /.main-wrapper -->
    
    <!-- /.page-wrapper -->
    
  
    <!-- /.side-wrapper -->
    <div class="side-overlay"></div>
    <!-- /.side-overlay -->

    @section('script')
    <script>
    var hostName = location.host.split(".");
    var domain = hostName.shift();

      var app = new Vue ({
          el: '#page-content',
          data: {
            file: [],
            property: [],
            status: true,
            totalProperty: 0,
            perPage: 9,
            currentPage: 1,
            franchise: [],
            id: 8,
            domain: domain,
            topagent: [],
            frachise: [],
            imageUri: 'https://www.remax.co.id/prodigy/papi/'

          },
          ready: function () {

          },
          methods: {
            fetchAgent: function () {
              var vm = this;
              axios.get('https://www.remax.co.id/prodigy/papi/membershiptop')
              .then(function(response){
                console.log(response)
                vm.topagent = response.data.data;
              })

            },

            fetchProperty: function (page) {
              var vm = this;

              function fetchFranchise() {
                return new Promise((resolve, reject) => {
                  axios.get('https://www.remax.co.id/prodigy/papi/franchise?filter[frofOfficeName]=' + "%27" + this.domain + "%27").then(function(result) {
                    resolve(result);
                    vm.fetchFranchise = result.data.data;
                    console.log(vm.fetchFranchise)
                  })
                })
              }

              fetchFranchise()
                .then(function(response) {
                  var options = {
                params: {
                  pageNumber: page,
                  pageSize: vm.perPage
                }
              }
              
              axios.get('https://www.remax.co.id/prodigy/papi/listing&filter[listOfficeId]=' + response.data.data[0]['id'] , options)
              .then(function(response){
                // console.log(response)
                
                  vm.property = response.data.data;
                  vm.file = response.data.linked.listFile;
                  vm.currentPage = page;
                  vm.totalProperty = response.data.status.totalRecords;
                  console.log(vm.totalProperty)
                
              })
              .catch(function(error){

              });
                })

              // console.log(this.domain)
              
            },

            formatRp: function(value) {
            var x = value;
  
                if (x >= 1000000000000){
                     x = Math.round((x / 1000000000000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' T';
                } else if (x >= 1000000000) {
                    x = Math.round((x / 1000000000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' M';
                } else if(x >= 1000000) {
                    x = Math.round((x / 1000000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' Jt';
                } else if(x > 0){
                    x = Math.round((x / 1000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' Rb';
                    
                }else {
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + '';
                }
          }


          },
          created: function () {
             this.fetchProperty(this.currentPage);
             this.fetchAgent(this.fetchFranchise);
           
          },
      });

    </script>
    @endsection
  </body>
</html>