<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.slider.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">

    <title>Search</title>
</head>

<body class="page-search" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header">@include('layout.header')</div>
    <!-- End Header -->

    <!-- Page Content -->
    <div id="page-content-search">
        <div class="container">
            <div class="wide_container_2" style="margin-left:0;margin-right:0;">

                <div class="tabs">
                    <header class="col-md-7 col-xs-12 no-pad">

                        <div class="location-map col-sm-4 col-xs-4">
                            <div class="input-group">
                                <input type="text" class="form-control" id="address-map"
                                       placeholder="Where do you want to live ?" v-model="query" @keyup.enter="lookupSearch">
                            </div>
                        </div>

                        {{--<pre> @{{ $data }} </pre>--}}

                        <div class="select-block col-sm-2 col-xs-2">
                            <select class="dropdown-input" v-model="bedroom">
                                <option value="">Beds</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>

                        <div class="select-block col-sm-2 col-xs-2 ">
                            <select class="dropdown-input">
                                <option>Type</option>
                                <option>For Sale</option>
                                <option>For Rent</option>
                            </select>
                        </div>

                        <div class="select-block col-md-3 col-xs-4 last">
                            <a class="options-button" id="toggle-link">More Filters</a>
                        </div>

                        <div class="options-overlay col-md-offset-4 col-sm-offset-5 col-sm-7" id="hidden_content"
                             style="display: none;">
                            <div class="row">
                                <div class="col-xs-6 top-mrg">
                                    <div class="internal-container features">
                                        <div class="form-group">
                                            <label>Minimum Square Footage:</label>
                                            <input type="text" class="form-control" placeholder="Enter an Amount">
                                        </div>
                                        <label>Building Features:</label>
                                        <section class="block">
                                            <section>
                                                <ul class="submit-features">
                                                    <li>
                                                        <div class="checkbox"><label><input
                                                                        type="checkbox">Elevator</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Gym</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Pool</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Lorem Ipsum</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </section>
                                        </section>
                                    </div>
                                </div>
                                <div class="col-xs-6 top-mrg">
                                    <div class="internal-container features">
                                        <label>Property Features:</label>
                                        <section class="block">
                                            <section>
                                                <ul class="submit-features">
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Air
                                                                conditioning</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input
                                                                        type="checkbox">Balcony</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Garage
                                                                Internet</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input
                                                                        type="checkbox">Dishwasher</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Washer
                                                                Mashine</label></div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox"><label><input type="checkbox">Some Lorem
                                                                Ipsum</label></div>
                                                    </li>
                                                </ul>
                                            </section>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div><!-- options-overlay -->
                    </header><!-- end header -->


                    <ul class="tab-links col-md-5 col-xs-12">

                        <li class="col-lg-6  col-md-4 col-xs-4 no-pad active"><a href="#tab1"
                                                                                 class="map2"><img
                                        src="{{ asset('assets/img/map.png') }}" alt=""/>Map</a></li>


                        <li class="col-lg-6 col-md-4 col-xs-4 no-pad"><a href="#tab2"><img
                                        src="{{ asset('assets/img/grid.png') }}"
                                        alt=""/>Grig</a></li>
                    </ul>

                    <!-- tab-links -->
                    <div class="tab-content">
                        <div id="tab1" class="tab" style="display: block;">
                            <div class="sidebar col-sm-5 col-xs-12">
                                <!-- Map -->
                                <div id="map"></div>
                                <!-- end Map -->
                            </div><!-- sidebar -->
                            <div class="content col-sm-7 col-xs-12">
                                <!-- Range slider -->
                                <div class="col-xs-12">
                                    <div class="row">
                                        <form method="post">
                                            <div class="col-md-3 col-sm-4">
                                                <div class="form-inline">
                                                    <label class="top-indent">Price Range:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8">
                                                <div class="form-group">
                                                    <div class="price-range price-range-wrapper">
                                                        <input class="price-input" type="text" name="propertyPrice"
                                                               value="0;5570000000">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- row -->
                                </div>    <!-- explore_grid -->
                                <div class="wide-2" id="listing-property">
                                    <div class="col-xs-12" style="overflow-y: scroll;height: 68vh;">
                                        <div class="row m0">
                                            <span class="help-block-danger" v-if="error"> @{{ error }} </span>

                                            <span class="help-block-danger" v-if="status">

                                                @{{ status }}

                                            </span>

                                            <div class="search-map-wrap" v-for="prop in property">
                                                <div class="house-card buy">

                                                    <a href="#">
                                                        <div class="image-wrap"
                                                        style="background-image: url('images/Not_available.png')"  >
                                                            <label>DIJUAL</label>
                                                        </div>
                                                    </a>

                                                    <div class="content-wrap">
                                                        <a href="#">
                                                            <div class="title">
                                                                @{{ prop.listTitle }}
                                                            </div>
                                                        </a>

                                                        <div class="price">
                                                            @{{ formatRp(prop.listListingPrice) }}
                                                        </div>
                                                        <div class="desc">
                                                            @{{ prop.listDescription }}
                                                        </div>
                                                        <div class="amenities">
                                                            <div class="row m0">
                                                                <div class="w-70 pull-left">
                                                                    <div class="icon-text">
                                                                        <i class="fa fa-bed" v-if="prop.listBedroom">
                                                                            @{{ prop.listBedroom }}
                                                                        </i>
                                                                        <i class="fa fa-bed" v-else>
                                                                            -
                                                                        </i>
                                                                    </div>

                                                                    <div class="icon-text">
                                                                        <i class="fa fa-shower"
                                                                           v-if="prop.listBathroom">@{{ prop.listBathroom }}</i>
                                                                        <i class="fa fa-shower" v-else> - </i>
                                                                    </div>
                                                                    <div class="icon-text">
                                                                        <i class="fa fa-area-chart"
                                                                           v-if="prop.listLandSize">@{{ prop.listLandSize }}
                                                                            m2 </i>
                                                                        <i class="fa fa-area-chart"
                                                                           v-else> @{{ prop.listLandSize }} m2 </i>


                                                                    </div>
                                                                </div>
                                                                <div class="w-30 pull-right">
                                                                    <a :href="'property/' + prop.id"
                                                                       class="l-button">More Info</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="location row m0">
                                                            <i class="fa fa-map-marker">@{{ prop.mctyDescription }}</i>

                                                            <div class="text">
                                                                <span v-for="cty in city"
                                                                      v-if="cty.id == prop.links.listCityId">@{{ cty.mctyDescription }}</span>
                                                                <span>@{{ prop.listStreetName }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- end container -->
                                </div>    <!-- end wide-2 -->


                            </div>    <!-- content -->
                        </div>


                        <div id="tab2" class="tab">
                            <div class="col-xs-12 content_2">
                                <div class="col-md-10 col-md-offset-1">
                                    <!-- Range slider -->
                                    <div class="explore_grid">
                                        <div class="row">
                                            <div class="explore col-xs-12">
                                                <h2>Properties for rent</h2>
                                            </div>
                                            <form method="post">
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="form-inline">
                                                        <label class="top-indent">Price Range:</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                    <div class="form-group">
                                                        <div class="price-range price-range-wrapper"></div>
                                                    </div>
                                                </div>
                                                <div class="select-block no-border pull-right col-sm-2 col-xs-12">

                                                    <select class="selection">
                                                        <option>Sort By:</option>
                                                        <option>Date</option>
                                                        <option>Price</option>
                                                        <option>Type</option>
                                                    </select>

                                                </div>    <!-- select-block -->
                                            </form>
                                        </div><!-- row -->
                                    </div>

                                    <div class="wide-2" id="listing-property">
                                        <div class="col-xs-12" style="overflow-y: scroll;height: 68vh;">
                                            <div class="row m0">
                                                <span class="help-block-danger" v-if="error"> @{{ error }} </span>
                                                <span class="help-block-status" v-if="status"> @{{ status }} </span>

                                                <div class="search-map-wrap" v-for="prop in property">
                                                    <div class="house-card buy">

                                                        <a href="#">
                                                            <div class="image-wrap"
                                                                 style="background-image: url('images/Not_available.png')">
                                                                <label>DIJUAL</label>
                                                            </div>
                                                        </a>

                                                        <div class="content-wrap">
                                                            <a href="#">
                                                                <div class="title">
                                                                    @{{ prop.listTitle }}
                                                                </div>
                                                            </a>

                                                            <div class="price">
                                                                @{{ formatRp(prop.listListingPrice) }}
                                                            </div>
                                                            <div class="desc">
                                                                @{{ prop.listDescription }}
                                                            </div>
                                                            <div class="amenities">
                                                                <div class="row m0">
                                                                    <div class="w-70 pull-left">
                                                                        <div class="icon-text">
                                                                            <i class="fa fa-bed" v-if="prop.listBedroom">
                                                                                @{{ prop.listBedroom }}
                                                                            </i>
                                                                            <i class="fa fa-bed" v-else>
                                                                                -
                                                                            </i>
                                                                        </div>

                                                                        <div class="icon-text">
                                                                            <i class="fa fa-shower"
                                                                               v-if="prop.listBathroom">@{{ prop.listBathroom }}</i>
                                                                            <i class="fa fa-shower" v-else> - </i>
                                                                        </div>
                                                                        <div class="icon-text">
                                                                            <i class="fa fa-area-chart"
                                                                               v-if="prop.listLandSize">@{{ prop.listLandSize }}
                                                                                m2 </i>
                                                                            <i class="fa fa-area-chart"
                                                                               v-else> @{{ prop.listLandSize }} m2 </i>


                                                                        </div>
                                                                    </div>
                                                                    <div class="w-30 pull-right">
                                                                        <a :href="'property/' + prop.id"
                                                                           class="l-button">More Info</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="location row m0">
                                                                <i class="fa fa-map-marker">@{{ prop.mctyDescription }}</i>

                                                                <div class="text">
                                                                <span v-for="cty in city"
                                                                      v-if="cty.id == prop.links.listCityId">@{{ cty.mctyDescription }}</span>
                                                                    <span>@{{ prop.listStreetName }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- end container -->
                                    </div>    <!-- end wide-2 -->


                                </div>


                            </div>

                        </div><!-- tab-content -->
                    </div><!-- tabs -->
                </div>
            </div>
        </div><!-- end Page Content -->

        <!-- Start Footer -->
        <!-- End Footer -->
    </div>
</div>
<!-- End Modal login, register, custom gallery -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBt6Pwo89NTFbXWEtrcEsL14M6af-gSjfQ&libraries=places"></script>

<script src="{{ asset('js/vue.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/infobox.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.dependClass-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/draggable-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/markerwithlabel_packed.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/location.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jshashtable-2.1_src.js') }}"></script>


<script>
    $('.selection').selectize(
            {
                sortField: 'text'
            }
    );
</script>

<script>
    // coordinates for current location
    var _latitude = -6.225673;
    var _longitude = 106.808481;
    createHomepageGoogleMap(_latitude, _longitude);
</script>



<script>
    new Vue({
        el: '#page-content-search',
        data: {
            error: '',
            baseUrl: 'http://prodigy.intelligence.id/',
            status: 'Pleasee wait..............',
            loading: false,
            property: [],
            city: [],
            beds: [],
            file: [],

            query: '',
            bedroom: ''

        },

        created: function(){
            this.status = '';
            this.lookupSearch()
        },

        watch: {
            query: function () {
                if (this.query.length >= 3) {
                    this.lookupSearch();
                }
            },

            bedroom : function (){
                if(this.bedroom.length >= 1){
                    this.lookupSearch();
                }
            }
        },

        methods: {

            lookupSearch: _.debounce(function () {
                var vm = this;

                vm.error = '';
                vm.city = [];
                vm.property = [];
                vm.file = [];
                vm.loading = true;



                axios.get('http://prodigy.intelligence.id/papi/listing?filter[mctyDescription]='+ '"%'  +vm.query  + '%"'+'&filter[listBedroom]='+ '"%' + vm.bedroom + '%"')
                        .then(function (response) {
                            if(response.data.data == null){
                                vm.error = 'Property not found , try with different keyword !';
                            }else {
                                Vue.set(vm.$data, 'property', response.data.data);
                                vm.city = response.data.linked.listCityId;
                                vm.file = response.data.linked.listFile;
                            }
                        })
            }, 500),

            formatRp: function (value) {
                var x = (0 + value.replace(',', ''));
                if (x > 1000000000) {
                    x = Math.round((x / 1000000000));
                    return x = 'Rp' + ' ' + x + ' M';
                } else {
                    x = Math.round((x / 1000000));
                    return x = 'Rp' + ' ' + x + ' JT';
                }
            }
    }
    });
</script>

<script>
    if ($(".price-input").length > 0) {
        $(".price-input").each(function () {
            var vSLider = $(this).slider({
                from: 0,
                to: 10000000000,
                smooth: true,
                round: 0,
                step: 10000000,
                dimension: '.00'
            });
        });
    }
</script>



<!--[if gt IE 8]> -->
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>
<![endif]-->
</body>
</html>

