<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleHttpClient;

class ContactController extends Controller
{
    public function getData()
    {
        $client = new GuzzleHttpClient(['base_uri'=>'https://www.remax.co.id/prodigy/','verify' => false]);
        $getClient = $client->get('papi/company/1');
        $body = $getClient->getBody();
        $body = \GuzzleHttp\json_decode($body, false);

        return view('contact_us',compact('body'));
    }
}
