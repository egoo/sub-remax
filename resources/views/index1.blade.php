<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/js/slick-1.6.0/slick/slick.css') }}">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />
    <title>Remax Indonesia</title>
    <style>
    .page-homepage body {
        height: 100%;
        overflow: hidden;
    }

    .bank-partner {
        font-size: 24px;
        padding-top: 5px;
        padding-bottom: 0px;
        text-align: center;
        top: 60%;
        left: 50%;
    }
    .text-search {
        font-size: 16px;
    }
    .bank-partner img {
            margin: 0 auto;
        height: 55px;
    }

    .advSearch-wrap img {
        margin: 0 auto;
        height: 50px;
    }

    .wrapper-icon {
        position: absolute;
        bottom: 0%;
        background: white;
        width: 100%;
        padding: 0 5px;

    }

    .slick-slide .slick-active {
        width: 150px !important;
        display: inline-block;
    }
    .text-a {
                display: inline-block;width: 202px;
            }

      @media only screen and (width: 320px){
        .text-a {
                display: inline-block;width: 104px;
         }
    }
       @media only screen and (width: 375px){
        .text-a {
                display: inline-block;width: 116px;
         }
    }

      @media only screen and (width: 360px){
        .text-a {
                display: inline-block;width: 115px;
            }
    } 
       @media only screen and (width: 412px){
        .text-a {
                display: inline-block;width: 130px;
         }
        }
        @media only screen and (width: 414px){
        .text-a {
                display: inline-block;width: 130px;
         }
        }

</style>
</head>

<body class="page-homepage" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="menu-wht"> @include('layout.header')</div>
    <!-- End Header -->
    <!-- Page Content -->
    <div>
        <div class="home-section">
            <div class="home-center">
                <div class="tabs-wrap pos-relative">
                    <ul class="row m0">
                        <li><a style="margin-left: 3px;" href="#buyForm" class="buy active">{{ $lg->data[0]->lsclName }}</a></li>
                        <li><a href="#rentForm" class="rent">{{ $lg->data[1]->lsclName }}</a></li>
                    </ul>

                    <a style="margin-top:7px;margin-right:4px;" href="{{ url('properties') }}" class="adv-search">Advanced Search </a>
                </div>

                <div class="search-wrap" >

                   <form action="{{ route('search.home') }}" id="buyForm">
                        <div class="row m0">
                           
                            <input type="text" placeholder="Find your dream home" class="form-control pull-left text-search" 
                                   name="buySearch">
                            <button type="submit" class="btn btn-small btn-primary pull-left">Go</button>
                         
                        </div>
                    </form>

                    <form action="{{ route('search.home') }}" id="rentForm" class="hideit">
                        <div class="row m0">
                            <input type="text" placeholder="Find your dream home" class="form-control pull-left text-search"
                                   name="rentSearch">
                            <button type="submit" class="btn btn-small btn-primary pull-left">Go</button>
                        </div>
                    </form>


                </div>
            </div>

            <div class="wrapper-icon" style="background: white;">
                <div class="bank-partner">
                <div class="bankpartner-slide js-bankpartner-slide">
                    @foreach($body->data as $data)
                        @foreach($body->linked->mbnkFileId as $linked)
                            @if($data->links->mbnkFileId == $linked->id)
                            <div class="text-a">
                                <div style="width: 100%;text-align: center;">
                                    <a href="{{ $data->mbnkUrl}}" target="_blank">
                                        <img style="background: white;"
                                             src="{{ $uri.$linked->filePreview.'?size=121,53' }}" alt="">
                                    </a>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick-1.6.0/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>


<script>
    $(".tabs-wrap ul li a").on("click", function () {
        var href = $(this).attr("href");
        $(".tabs-wrap ul li a").removeClass("active");
        $(this).addClass("active");

        $(".search-wrap form").addClass("hideit");
        $(href).removeClass("hideit");
    });

    $('.js-bankpartner-slide').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 8,
        slidesToScroll: 1,
        customPaging: function (slider, i) {
            return '<div class="bankpartner-paging"></div>';
        },
        autoplay: true,
        speed: 1000,
        variableWidth: true,
        focusOnSelect: false,
        prevArrow: '<div style="display:none;"></div>',
        nextArrow: '<div style="display:none;"></div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: false
                }
            }]
    });
</script>
<!-- End - Custom Javascript -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99368247-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>