<!DOCTYPE html>

<style>
    input[type=checkbox] {
        -webkit-appearance: checkbox;
    }

</style>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.slider.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/customsas.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/js/slick-1.6.0/slick/slick.css') }}">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,800" rel="stylesheet">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />



    <title>Search</title>

    <style>

     .tool_properties {
        margin-top: 0px;
        margin-bottom: 17px;
        float: right;
        border-radius: 0px;
        padding-bottom: 26px;
        padding-right: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
     }  
       .tool_properties1 {
        margin-top: 0px;
        margin-bottom: 17px;
        float: right;
        border-radius: 0px;
        padding-bottom: 20px;
 
        padding-right: 15px;
        margin-right: 15px;
        margin-bottom: 0px;
     }  


     .logo-remax {
        background-size: cover; 
        width: 129px; 
        height: 56px;
        margin-top: -14px;
        margin-left: -58px;
    }
     @media only screen and (max-width: 768px){
        .logo-remax {
                background-size: cover; 
                width: 153px; 
                height: 72px;
                margin-top: -17px;
                margin-left: -43px;
            }
    } 
    #scroll-properties{
          overflow-y: scroll;
          height: 87vh;
    }

    @media only screen and (max-width: 1198px){
        .col-img-12 {
            width: 100%;
        }
    } 

        @media only screen and (max-width: 1300px){
        .left-share {
            margin-left: -50px;
        }
        .top-agent {
            padding-top: 25px;
        }
    }     

 @media only screen and (max-width: 1024px){
      .tulisan{
            font-size: 8px;
      }
      .tulisan button {
            font-size: 8px;
      }
    }   
 @media only screen and (max-width: 1255px){
      .tulisan{
            font-size: 8px;
      }
   
    }   
    .text-icon {
        font-size: 11px;
    }
       @media only screen and (max-width: 1300px){
        .text-icon {
        font-size: 9px;
        }
    }   
    .col-centered{
    float: none;
    margin: 0 auto;
    }
    .font-kecil{
        font-size: 10px;
    }
    .placeholder::-webkit-input-placeholder{
        font-size: 0.8em;
    }
    .placeholder::-moz-placeholder{
        font-size: 0.8em;
    }
    .placeholder1::-webkit-input-placeholder{
        font-size: 0.8em;
    }
    .placeholder1::-moz-placeholder{
        font-size: 0.8em;
    }
     @media only screen and (max-width: 1366px){
           .placeholder::-webkit-input-placeholder{
        font-size: 0.6em;
            }
            .placeholder::-moz-placeholder{
                font-size: 0.6em;
            }
            .placeholder1::-webkit-input-placeholder{
                font-size: 0.6em;
            }
            .placeholder1::-moz-placeholder{
                font-size: 0.6em;
            }
    }       
    @media only screen and (max-width: 480px){
           .placeholder::-webkit-input-placeholder{
        font-size: 1em;
            }
            .placeholder::-moz-placeholder{
                font-size: 1em;
            }
            .placeholder1::-webkit-input-placeholder{
                font-size: 1em;
            }
            .placeholder1::-moz-placeholder{
                font-size: 1em;
            }
    }
</style>
</head>

<body class="page-search" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->


    <div id="header" class="prop">
<nav class=" navbar-default">

  <div class="container-fluid" id="top">
  
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed tool_properties" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" >
        <p></p>
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <button type="button" class="navbar-toggle collapsed tool_properties1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false" >
        <p></p>
        <span></span>
        &nbsp;<span class="fa fa-search"></span>
       </button>
      <a class="navbar-brand col-xs-offset-2" href="{{ route('/') }}" title="" rel="home" >
                   <img src="{{ asset('/assets/img/new-remax-log-fix2.png')}}" class="logo-remax">
      </a>
    </div>

    <div class=" primary start main-menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
                    @if($header->data)
                        @foreach($header->data as $data)                   
                            <li class="{{ Request::is($data->wbmnAPI) ? 'active' : '' }}">
                                <a href="{{ route($data->wbmnAPI) }}" style="font-weight: normal;">{{ $data->wbmlName }}</a>
                            </li>      
                        @endforeach
                    @endif
                        <li>
                            <a href="https://www.remax.co.id/admin/">
                                <i class="fa fa-lock" style="font-size:20px;"></i>
                            </a>
                        </li>
                    @if($language->data)
                        @foreach($language->data as $lg)
                                <li>
                                    <a style="padding-left: 5px;padding-right: 5px;" href="?language={{$lg->langCode }}">
                                    @foreach($language->linked->langFileId as $linked)
                                        @if($lg->links->langFileId == $linked->id)
                                            <img style="width: 30px; height: 15px;" src="{{ 'https://www.remax.co.id/prodigy/papi/'.$linked->filePreview.'?size=30,32' }}" alt="">
                                        @endif
                                    @endforeach
                                    </a>
                                </li>
                        @endforeach
                    @endif
      </ul>
    </div>
  </div>
  
</nav>
</div>
   

   
    <!-- End Header -->

    <!-- Page Content -->
    <div id="page-content-search">
        <div class="container">
            <div class="wide_container_2 " style="margin-left:0;margin-right:0;font-size: 11px;">
                <div class="tabs">


                    <header class="col-md-12 col-sm-12 col-xs-12 no-pad form-group hidden-xs">

                        {{-- <div class="select-block select-block-md1 col-md-1 ">
                        <input type="text" id="address-map" class="form-input selectize-input placeholder1" placeholder="search location or property?" v-model="query" @keyup.enter="locateMap">
                        </div> --}}

                        <div class="select-block select-block-xs col-md-1 col-sm-1 col-xs-1">
                        <select class="form-control tulisan"
                                                         v-on:change="
                                                sortByValue(sortKey)" v-model="
                                                sortKey">
                                                <option value="" selected style="display:none;">Sort By:</option>
                                                <option value="reset" >Sort By:</option>
                                                <option value="newest">Newest</option>
                                                <option value="oldest">Oldest</option>
                                                <option value="expensive">highest price</option>
                                                <option value="cheapest">Lowest price</option>
                                                <option value="smallLandarea">Smallest Land Area</option>
                                                <option value="largeLandarea">Largest land area</option>
                                                <option value="smallBuildingarea">Smallest Building Area</option>
                                                <option value="largestBuildingarea">Largest Building Area</option>
                                               {{--  <option value="lowpriceMeters">Lowest Price/Meters</option>
                                                <option value="highpriceMeters">Highest Price/Meters</option> --}}

                        </select>

                        </div>

                         <div class="select-block select-block-md col-md-1 col-sm-1 col-xs-1">
                          
                         <input style="background-color: white;" type="text" name="" class="form-input selectize-input placeholder1 tulisan" width="" placeholder="Search by Property Location" width="" v-model="elastic" @keyup="multipleCall">
                        </div>
<!-- 
                       <div class="select-block select-block-md col-md-1 col-sm-1 col-xs-1">
                             <input style="background-color: white;" type="text" id="address-map" class="form-input selectize-input placeholder1"  v-model="query"@keyup="locateMap" value="test">
                              

                       </div> -->
                       <!--  <div class="search-wrap">
                        <a id="byProperty" >
                        <div class="select-block select-block-md col-md-1 col-sm-1 col-xs-1" >
                            

                            <input style="background-color: white;" type="text" id="address-map" name="" class="form-control selectize-input placeholder1" width="" placeholder="Search By Property" width="" v-model="elastic" @keyup.enter="multipleCall">
                        </div>
                        </a>
                       

                      
                        <a id="bylocation" class="hideit" >
                        <div class="select-block select-block-md col-md-1 col-sm-1 col-xs-1" >
                            

                            <input style="background-color: white;" type="text" id="address-map" class="form-control selectize-input placeholder1" placeholder="search by location?" v-model="query" @keyup.enter="locateMap">
                        </div>
                        </a>
                         </div> -->

                        <div class="select-block select-block-xs col-md-1 col-sm-1 col-xs-1">
                            <select class="form-control tulisan" v-model="bed" v-on:change="multipleCall">
                                <option value="" selected class="tulisan" style="display:none;"> Beds</option>
                                <option value=""> Beds</option>
                                <option value="1">1 Bedroom</option>
                                <option value="2">2 Bedroom</option>
                                <option value="3">3 Bedroom</option>
                                <option value="4">4 Bedroom</option>
                                <option value="5">5 + Bedroom</option>
                            </select>
                        </div>
{{-- <div class="select-block col-sm-2 col-xs-2">
                                <select class="form-control" tabindex="-1" style="display: none;"><option value="Beds" selected="selected"></option></select>
                                <div class="selectize-control selection single">
                                <div class="selectize-input items full has-options has-items">
                                <div data-value="Beds" class="item">Beds</div>
                                <input type="text" autocomplete="off" tabindex="" style="width: 4px; opacity: 0; position: absolute; left: -10000px;" readonly="readonly">
                                </div>
                                <div class="selectize-dropdown selection single" style="display: none; visibility: visible; width: 159px; top: 48px; left: 0px;">
                                <div class="selectize-dropdown-content"><div data-value="1" data-selectable="" class="option">1</div>
                                <div data-value="2" data-selectable="" class="option">2</div>
                                <div data-value="3" data-selectable="" class="option">3</div>
                                <div data-value="Beds" data-selectable="" class="option selected">Beds</div>
                                </div>
                                </div>
                                </div>
                            </div> --}}

                        <div class="select-block select-block-xs col-md-1 col-xs-1 col-sm-1">
                            <select class="form-control tulisan" v-model="filterCategory" v-on:change="multipleCall" >
                                <option value="" selected style="display: none;">Type</option>
                                <option value="">Type</option>
                                <option :value="cat.id" v-for="cat in categoryType">@{{ cat.lsclName }}</option>
                            </select>
                        </div>

                         {{-- <div class="select-block select-block-xs">
                            <select class="form-control" v-model="propstype" v-on:change="multipleCall">
                                <option value="" selected style="display: none;">Property Type</option>
                                <option :value="proptype.id" v-for="proptype in propertyType">@{{ proptype.prtlName }}</option>
                            </select>
                        </div> --}}

                        {{-- <div class="select-block select-block-md3 col-md-2 col-xs-2 col-sm-2">
                            <a class="options-button" id="toggle-link-property">Property Type</a>
                        </div>

                        <div class="select-block select-block-md3 col-md-2 col-sm-2 col-xs-2">
                            <a class="options-button" id="toggle-link">Features</a>
                        </div>
 --}}
                        <div class="select-block select-block-xs col-md-1 col-xs-1 col-sm-1">
                             <!-- <a id="toggle-link-property" >
                            <select class="form-control" style="font-size: 11px;">
                  

                               <option value="" selected style="display: none"> Property Type</option>
                               <option disabled > Property Type</option>

                            </select>
                            </a> -->

                             <div class="dropdown">
                                <button class="form-control dropdown-toggle tulisan" type="button" data-toggle="dropdown" style="text-align: left;margin-left: 3.5px;">Property Type 
                                <span style="margin-top: 4px;" class="caret pull-right hidden-xs hidden-sm"></span></button>
                                <ul class="dropdown-menu col-xs-12">
                                <li v-for="props in propertyType" @click="multipleCall">
                                <div class="checkbox" style="margin-left: 3px;">
                                      <label> <input type="checkbox" class="form-control" v-model="checkedProperty"
                                                                      :value="props.id" > @{{ props.prtlName }}</label>
                                </div>
                                                      
                                </li>
                                <li class="pull-right">
                                    <a  style="font-size: 11px;color: red;" @click="resetProperty">Reset </a>
                                </li>
                              </div>


                        </div>

                        <div class="select-block select-block-xs col-md-1 col-xs-1 col-sm-1">
                             <!-- <a id="toggle-link-property" >
                            <select class="form-control" style="font-size: 11px;">
                  

                               <option value="" selected style="display: none"> Property Type</option>
                               <option disabled > Property Type</option>

                            </select>
                            </a> -->

                             <div class="dropdown">
                                <button class="tulisan form-control dropdown-toggle tulisan" type="button" data-toggle="dropdown" style="text-align: left;margin-left: 3.5px;">Features 
                                <span style="margin-top: 4px;" class="caret pull-right hidden-xs hidden-sm"></span></button>
                                <ul class="dropdown-menu col-xs-12">
                                <li v-for="fac in facility" @click="multipleCall">
                                <div class="checkbox" style="margin-left: 3px;">
                                      <label><input type="checkbox" v-model="checkedFacility"
                                                                      :value="fac.id"> @{{ fac.fctlName }}</label>
                                </div>
                                                     
                                </li>
                                 <li class="pull-right">
                                    <a  style="font-size: 11px;color: red;" @click="resetFeature">Reset </a>
                                </li>
                              </div>


                        </div>
                        <!--   <div class="select-block select-block-xs col-md-1 col-sm-1 col-xs-1">
                             <a id="toggle-link" >
                            <select class="form-control" style="font-size: 11px;">
                               <option value="" selected style="display: none"> Features</option>
                               <option disabled> Features</option>

                            </select>
                            </a>
                        </div> -->


                        <div class="select-block select-block-xs col-md-1 col-xs-1 col-sm-1">

                            <select class="tulisan form-control" v-model="minPrice" v-on:change="multipleCall" >
                                <option value="" selected style="display: none" > Min Price</option>
                                <option value=""> Min Price</option>
                                <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                            </select>
                        </div>

                        <div class="select-block select-block-xs col-md-1 col-xs-1 col-sm-1">

                            <select class="tulisan form-control" v-model="maxPrice" v-on:change="multipleCall">
                                <option value="" selected style="display: none"> Max Price</option>
                                <option value="100000000000"> Max Price</option>
                                <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                            </select>
                        </div>


                        <div class="select-block select-block-sm col-md-1 col-xs-1 col-sm-1" >
                            <input type="number" name="" class="form-input selectize-input placeholder tulisan" placeholder="Area Size (m2)" width="10px" v-model="landSize" @keyup.enter="multipleCall">
                        </div>
                        <div class="select-block select-block-sm col-md-1 col-xs-1 col-sm-1">
                            <input type="number" name="" class="form-input selectize-input   placeholder tulisan" placeholder="Building Size (m2)" v-model="buildingSize" @keyup.enter="multipleCall">
                        </div>
 {{-- <div class="select-block select-block-md2 col-md-1">
                                <a id="toggle-link" style="color: #E21A22" @click="resetFilter">Reset All</a>
                            </div> --}}
  <div class="select-block select-block-sm col-md-1">
                            {{-- <input type="text" name="" class="form-input selectize-input" placeholder="Building Size" v-model="buildingSize" @keyup="multipleCall"> --}}
                            <button class="form-control " style="color: red; font-size:12px;" @click="resetAllSearch">Reset All </button>
                        </div>

                        <div class="options-overlay col-md-offset-5 col-xs-7 col-sm-offset-5 col-md-7 col-sm-7" id="hidden_content"
                             style="display: none;margin-left: 45%;position: absolute;z-index: 99999">

                             <!-- Filter reset -->
{{--                             <div class="pull-right">
                                <a id="toggle-link" style="color: #E21A22" @click="resetFilter">Reset</a>
                            </div> --}}

                            <!-- Body Filter -->
{{--                             <div class="row">
                                <div class="col-xs-12 col-sm-12 top-mrg">
                                    <div class="internal-container features">
                                        <div class="col-xs-6 top-mrg"> --}}
                                            {{-- <div class="form-group">
                                                <label>Area Size :</label>
                                                <input type="text" class="form-control" placeholder="Enter an Amount"
                                                       v-model="landSize" style="padding:5px 10px;margin-bottom: 5px;" @keyup="multipleCall">

                                                <label>Building Size :</label>
                                                <input type="text" class="form-control" placeholder="Enter an Amount"
                                                       v-model="buildingSize" style="padding:5px 10px;margin-bottom: 5px;" @keyup="multipleCall">

                                                <section class="block">
                                                    <section>
                                                        <label> Min Price </label>
                                                        <select class="form-control" v-model="minPrice"
                                                                style="padding: 5px 10px;margin-bottom: 5px;" v-on:change="
                                                        multipleCall">
                                                        <option value="0">Min</option>
                                                        <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                                                        </select>
                                                    </section>

                                                    <section>
                                                        <label> Max Price </label>
                                                        <select class="form-control" name="" id=""
                                                                style="padding: 5px 10px;margin-bottom: 5px;"
                                                                v-model="maxPrice" v-on:change="multipleCall">
                                                        <option value="100000000000">Max</option>
                                                        <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                                                        </select>
                                                    </section>

                                                </section>

                                            </div>
                                        </div>
 --}}   
                                    
                                         <div class="row">
                                        <div class="col-xs-12 top-mrg">
                                            <label>Property Features:</label>
                                            <section class="block">
                                                <section>
                                                    <ul class="submit-features">
                                                        <li v-for="fac in facility" @click="multipleCall">
                                                        <label><input type="checkbox" v-model="checkedFacility"
                                                                      :value="fac.id">@{{ fac.fctlName }}</label>
                                                        </li>
                                                    </ul>
                                                </section>
                                            </section>
                                        </div>
                                    </div>

                                      <div class="pull-right">
                                      <br />
                                <a id="toggle-link" class="selectize-input" style="color: #E21A22" @click="resetFeature">Reset Features</a>
                                {{-- <button class="btn btn-default selectize-input">Reset</button> --}}
                            </div>
                                {{-- </div>


                            </div> --}}
                        </div><!-- options-overlay -->

                                                <div class="options-overlay col-xs-7 col-xs-offset-12 col-md-offset-4 col-sm-offset-5 col-sm-7" id="hidden_content_property"
                             style="display: none; margin-left: 37%;position: absolute; z-index: 99999">

                             <!-- Filter reset -->
{{--                             <div class="pull-right">
                                <a id="toggle-link" style="color: #E21A22" @click="resetFilter">Reset</a>
                            </div> --}}

                            <!-- Body Filter -->
{{--                             <div class="row">
                                <div class="col-xs-12 top-mrg">
                                    <div class="internal-container features">
                                        <div class="col-xs-6 top-mrg"> --}}
                                            {{-- <div class="form-group" >
                                                <label>Area Size :</label>
                                                <input type="text" class="form-control" placeholder="Enter an Amount"
                                                       v-model="landSize" style="padding:5px 10px;margin-bottom: 5px;" @keyup="multipleCall">

                                                <label>Building Size :</label>
                                                <input type="text" class="form-control" placeholder="Enter an Amount"
                                                       v-model="buildingSize" style="padding:5px 10px;margin-bottom: 5px;" @keyup="multipleCall">

                                                <section class="block">
                                                    <section>
                                                        <label> Min Price </label>
                                                        <select class="form-control" v-model="minPrice"
                                                                style="padding: 5px 10px;margin-bottom: 5px;" v-on:change="
                                                        multipleCall">
                                                        <option value="0">Min</option>
                                                        <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                                                        </select>
                                                    </section>

                                                    <section>
                                                        <label> Max Price </label>
                                                        <select class="form-control" name="" id=""
                                                                style="padding: 5px 10px;margin-bottom: 5px;"
                                                                v-model="maxPrice" v-on:change="multipleCall">
                                                        <option value="100000000000">Max</option>
                                                        <option v-for="price in priceRange"
                                                                :value="price">@{{ formatRp(price)}}</option>
                                                        </select>
                                                    </section>

                                                </section>

                                            </div>
                                        </div>
 --}}
                                    <div class="row">
                                        <div class="col-xs-12 top-mrg">
                                            <label>Property Type:</label>
                                            <section class="block">
                                                <section>
                                                    <ul class="submit-features">
                                                        <li v-for="props in propertyType" @click="multipleCall">
                                                        <label><input type="checkbox" v-model="checkedProperty"
                                                                      :value="props.id">@{{ props.prtlName }}</label>
                                                        </li>
                                                    </ul>
                                                </section>
                                            </section>
                                        </div>
                                    </div>

                            <div class="pull-right">
                                <a id="toggle-link-property" class="selectize-input" style="color: #E21A22" @click="resetProperty">Reset Property</a>
                                {{-- <button class="btn btn-default selectize-input">Reset</button> --}}
                            </div>
                                {{-- </div>


                            </div> --}}
                        </div><!-- options-overlay -->



                    <ul class="tab-links col-md-1 col-xs-12 col-sm-1 pull-right hidden-xs" >

                        <li class="col-lg-6  col-md-6 col-xs-12 col-sm-12 no-pad active"><a href="#tab1"
                                                                                 class="map2"><img
                                        src="{{ asset('assets/img/map.png') }}" alt=""/></a></li>


                        <li class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-pad"><a href="#tab2" class="map2"><img src="{{ asset('assets/img/grid.png') }}"
                                        alt=""/></a></li>
                    </ul>
                    </header><!-- end header -->



                    <div class="kito ">
                        <nav class=" navbar-default hidden-md hidden-lg">
                              <div class="container-fluid" id="top">
                                <div class=" primary start main-menu collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                  <ul class="nav navbar-nav navbar-right">
                                                
                                                    
                                        <li class="">
                                            <select class="form-control"
                                                         v-on:change="
                                                sortByValue(sortKey)" v-model="
                                                sortKey" style="font-size: 11px;">
                                                <option value="" selected style="display:none;">Sort By:</option>
                                                <option value="reset" >Sort By:</option>
                                                <option value="newest">Newest</option>
                                                <option value="oldest">Oldest</option>
                                                <option value="expensive">highest price</option>
                                                <option value="cheapest">Lowest price</option>
                                                <option value="smallLandarea">Smallest Land Area</option>
                                                <option value="largeLandarea">Largest land area</option>
                                                <option value="smallBuildingarea">Smallest Building Area</option>
                                                <option value="largestBuildingarea">Largest Building Area</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input  type="text" name="" class="form-input selectize-input placeholder1" width="" placeholder="Search by Property Location" width="" v-model="elastic" @keyup.enter="locateMap">
                                        </li>
                                        <li>
                                            <select class="form-control" v-model="bed" v-on:change="multipleCall" style="font-size: 11px;">
                                                <option value="" selected style="display:none;font-size: 11px;"> Beds</option>
                                                <option value=""> Beds</option>
                                                <option value="1">1 Bedroom</option>
                                                <option value="2">2 Bedroom</option>
                                                <option value="3">3 Bedroom</option>
                                                <option value="4">4 Bedroom</option>
                                                <option value="5">5 + Bedroom</option>
                                            </select>
                                        </li>  
                                         <li>
                                            <select class="form-control" v-model="filterCategory" v-on:change="multipleCall" style="font-size: 11px;">
                                                <option value="" selected style="display: none;">Type</option>
                                                <option value="">Type</option>
                                                <option :value="cat.id" v-for="cat in categoryType">@{{ cat.lsclName }}</option>
                                            </select>
                                         </li>   
                                         <li>
                                                <div class="dropdown">
                                                    <button class="form-control dropdown-toggle" type="button" data-toggle="dropdown" style="font-size:11px;text-align: left;margin-left: 3.5px;">Property Type 
                                                    <span style="margin-top: 4px;" class="caret pull-right hidden-xs hidden-sm"></span></button>
                                                    <ul class="dropdown-menu col-xs-12">
                                                    <li v-for="props in propertyType" @click="multipleCall">
                                                    <div class="checkbox" style="margin-left: 3px;">
                                                          <label style="font-size:11px;"> <input type="checkbox" class="form-control" v-model="checkedProperty"
                                                                                          :value="props.id" > @{{ props.prtlName }}</label>
                                                    </div>
                                                                          
                                                    </li>
                                                    <li class="pull-right">
                                                        <a  style="font-size: 11px;color: red;" @click="resetProperty">Reset </a>
                                                    </li>
                                                </div>
                                         </li> 
                                         <li>
                                              <div class="dropdown">
                                                <button class="form-control dropdown-toggle" type="button" data-toggle="dropdown" style="font-size:11px;text-align: left;margin-left: 3.5px;">Features 
                                                <span style="margin-top: 4px;" class="caret pull-right hidden-xs hidden-sm"></span></button>
                                                <ul class="dropdown-menu col-xs-12">
                                                <li v-for="fac in facility" @click="multipleCall">
                                                <div class="checkbox" style="margin-left: 3px;">
                                                      <label style="font-size:11px;"><input type="checkbox" v-model="checkedFacility"
                                                                                      :value="fac.id"> @{{ fac.fctlName }}</label>
                                                </div>
                                                                     
                                                </li>
                                                 <li class="pull-right">
                                                    <a  style="font-size: 11px;color: red;" @click="resetFeature">Reset </a>
                                                </li>
                                              </div>
                                         </li>  
                                         <li>
                                             <select class="form-control" v-model="minPrice" v-on:change="multipleCall" style="font-size: 11px;">
                                                <option value="" selected style="display: none" > Min Price</option>
                                                <option value=""> Min Price</option>
                                                <option v-for="price in priceRange"
                                                                                :value="price">@{{ formatRp(price)}}</option>
                                            </select>
                                         </li>   
                                         <li>
                                              <select class="form-control" v-model="maxPrice" v-on:change="multipleCall" style="font-size: 11px;">
                                                    <option value="" selected style="display: none"> Max Price</option>
                                                    <option value="100000000000"> Max Price</option>
                                                    <option v-for="price in priceRange"
                                                                                    :value="price">@{{ formatRp(price)}}</option>
                                              </select>
                                         </li>  
                                         <li>
                                            <input type="text" name="" class="form-input selectize-input placeholder" placeholder="Area Size (m2)" width="10px" v-model="landSize" @keyup.enter="multipleCall">
                                         </li>     
                                         <li>
                                             <input type="text" name="" class="form-input selectize-input   placeholder" placeholder="Building Size (m2)" v-model="buildingSize" @keyup.enter="multipleCall">
                                         </li>
                                         <li>
                                             <button class="form-control " style="color: red; font-size:12px;" @click="resetAllSearch">Reset All </button>
                                         </li>
                                  </ul>
                                </div>
                              </div>
                              
                            </nav>



                    </div>




                    <!-- tab-links -->
                    <div class="tab-content">
                        <div id="tab1" class="tab" style="display: block;">
                            <div class="sidebar col-sm-6 col-xs-12 hidden-xs">
                                <!-- Map -->
                                <div id="map"></div>
                                <!-- end Map -->
                            </div><!-- sidebar -->
                            <div class="content col-sm-6 col-xs-12 no-padding">
                                <div class="wide-2" id="listing-property">

                                    <div class="col-xs-12 mbl-list-properties" id="scroll-properties">

                                        <div class="row m0">

                                                <div class="container-404" v-if="error">
                                                    <div class="container">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="text-404" style="text-align: center">
                                                                    <div class="col-md-12 col-sm-12 no-page" style="text-align:center;margin-top:150px;">
                                                                        <span class="sorry">Oops.. Your search result not found !</span>
                                                                        <p style="text-align: center"> please search again using diffrent keywords.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>


                                            <div class="container-404 container-status" v-if="status">
                                                <div class="container">
                                                    <div class="text-404" style="text-align: center">
                                                        <div class=" no-page" style="text-align:center;margin-top: 150px;">
                                                            <img src="{{ asset('images/reload.gif') }}">
                                                            <br>
                                                            <span class="sorry">Finding the right property for you ....</span>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--<span class="help-block-danger" v-if="error"> Property Not Foundd !</span>--}}


                                            <!-- {{--@for($i=0;$i<10;$i++)--}}

                                            <div ref="marker" :id="'marker'+ prop.id" data-target="'marker'+ prop.id" class="search-map-wrap col-md-12"
                                                 v-for="prop in property">
                                                <a :href="'property/' + prop.listUrl" style="display:block;">

                                                    <div class="house-card buy">
                                                        <div class="image-wrap"
                                                             :style="{ backgroundImage: 'url(' + imageUri + fil.filePreview + ')'}"
                                                             v-for="fil in file"
                                                             v-if="fil.id == prop.links.listFile[0]"
                                                        >
                                                            <label :class="[prop.links.listListingCategoryId == 1 ? 'buy' : 'rent' ]">
                                                                @{{ prop.links.listListingCategoryId == 1 ? 'Sale' : 'Rent'   }} </label>
                                                        </div>

                                                        <div class="content-wrap">
                                                            <div class="title">
                                                                @{{ prop.listTitle }}
                                                            </div>


                                                            <div class="price">
                                                                @{{ prop.listListingPrice ? formatRp(prop.listListingPrice) : '' }}
                                                            </div>
                                                            <div class="desc">
                                                                @{{ prop.listDescription }}

                                                            </div>
                                                            <div class="amenities">
                                                                <div class="row m0">
                                                                    <div class="w-70 pull-left">

                                                                        <div class="icon-text">
                                                                            <i class="fa fa-bed"
                                                                               v-if="prop.listBedroom">
                                                                                @{{ prop.listBedroom }}
                                                                            </i>
                                                                            <i class="fa fa-bed" v-else>
                                                                                -
                                                                            </i>
                                                                        </div>

                                                                        <div class="icon-text">
                                                                            <i class="fa fa-shower"
                                                                               v-if="prop.listBathroom">@{{ prop.listBathroom }}</i>
                                                                            <i class="fa fa-shower" v-else> - </i>
                                                                        </div>

                                                                        <div class="icon-text">
                                                                            <i class="fa fa-area-chart"
                                                                               v-if="prop.listLandSize">@{{ prop.listLandSize }} m2</i>
                                                                            <i class="fa fa-area-chart"
                                                                               v-else> - m2 </i>
                                                                        </div>


                                                                        <div class="icon-text">
                                                                            <i class="fa fa-building"
                                                                               v-if="prop.listLandSize">@{{ prop.listBuildingSize }} m2</i>
                                                                            <i class="fa fa-building"
                                                                               v-else> - m2 </i>
                                                                        </div>



                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="location row m0">
                                                                <i class="fa fa-map-marker">@{{ prop.mctyDescription }}</i>

                                                                <div class="text">
                                                                <span v-for="cty in city"
                                                                      v-if="cty.id == prop.links.listCityId">@{{ cty.mctyDescription}}</span>
                                                                    <span v-if="prop.listStreetName">@{{ prop.listStreetName }}</span>
                                                                    <span v-else> - </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            {{--@endfor--}} -->

                                         {{--@for($i=0;$i<10;$i++)--}}

                                    
                                    <div class="markers" ref="marker" :id="'marker'+ prop.id" :data-target="'marker'+ prop.id"
                                             v-for="prop in propertys">
                                            <a :href="'/property/' + prop.listUrl" style="display:block;" target="_blank">

                                         

                                                    <div class="row white" style="box-shadow:2px 3px 5px 0 rgba(0,0,0,0.2); border-radius:3px; margin-bottom: 8px;">

                                                     <div class="item-info col-md-12 col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;padding-bottom: 0px;padding-top: 0px;">
                                                      <div class="col-md-3 col-img-12">



                                                       <div class="house-card1 buy1" v-if="prop.links.listFile" >
                                                            <div class="image-wrap1"
                                                             :style="{ backgroundImage: 'url(' + imageUri + fil.filePreview + ')'}"
                                                             v-for="fil in file"
                                                             v-if="fil.id == prop.links.listFile[0]"
                                                        >
                                                           <label :class="[prop.links.listListingCategoryId == 1 ? 'buy1' : 'rent1' ]">
                                                                        @{{ prop.links.listListingCategoryId == 1 ? 'For Sale' : 'For Rent'   }} </label>
                                                            </div>
                                                        </div>
                                                        <div v-else>
                                                             <img src="{{ asset('images/Not_available.jpg') }}" class="img-responsive" style="height:147px;width: 100%">
                                                        </div>
                                                   </div>
                                                <div class="col-md-9"  > 
                                                   <div class="border1" >
                                                   <div class="row">
                                                                <div class="col-md-7 col-xs-7">
                                                                    <h4 style="font-size:1.7vh;color: #000">
                                                                  
                                                                            @{{ prop.listTitle | truncate(27, '...')}}
                                                                       
                                                                   </h4>
                                                                </div>
                                                                <div class="col-md-5"> 
                                                                     <div class="pull-right">
                                                                            <h4 style="font-family:'Montserrat', sans-serif ;color: #E21A22;font-size: 2.2vh;">@{{ prop.listListingPrice ? formatRp(prop.listListingPrice) : '' }}</h4>
                                                                    </div>
                                                                </div>
                                                     </div>      
                                                            <!-- <p class="team-color" style="font-size: 10px;">525 W 28th St, New York, NY 10001</p> -->
                                                            <div class="row">
                                                                <div class="col-xs-12" v-if="prop.listDescription">
                                                                    <p style="font-size: 1.3vh;" >@{{ prop.listDescription | truncate(50, '...')}} </p><label style="color:red;font-size: 10px;" >Read More...</label>
                                                                 
                                                                    <div v-else>
                                                                    <p style="font-size: 12px;" > - </p>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                                    
                                                                    <span class="text-icon"  v-if="prop.listBedroom" style="color:#000;"><img src="/assets/img/bedroom.png" alt="" width="20px" height="20px"> @{{ prop.listBedroom }} </span>
                                                                    <span style="font-size: 1.7vh"  v-else> - </span>
                                                                </div>
                                                                <div class="col-md-3 col-xs-3  col-sm-3 ">
                                                                   
                                                                    <span class="text-icon"  v-if="prop.listBathroom" style="color:#000;"><img src="/assets/img/bathroom.png" alt="" width="20px" height="20px"> @{{ prop.listBathroom }}</span>
                                                                     <span style="font-size: 1.7vh"  v-else> - </span>
                                                                </div>
                                                                <div class="col-md-3 col-xs-3 col-sm-3 ">
                                                                    
                                                                    <span  class="text-icon" v-if="prop.listLandSize" style="color:#000;"><img src="/assets/img/map1.png" alt="" width="20px" height="20px"> @{{ prop.listLandSize }} m2</span>
                                                                     <span style="font-size: 1.7vh"  v-else > - m2</span>
                                                                </div>
                                                                <div class="col-md-3 col-xs-3 col-sm-3">
                                                                    
                                                                    <span  class="text-icon"
                                                                               v-if="prop.listLandSize" style="color:#000;"><img src="/assets/img/square.png" alt="" width="20px" height="20px"> @{{ prop.listBuildingSize }} m2</span>
                                                                    <span style="font-size: 1.7vh"  v-else> - m2</span>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="location row m0">
                                                                <div class="text-city row col-md-12 col-xs-12" style="margin-top: -15px;">
                                                                <i class="fa fa-map-marker">@{{ prop.mctyDescription }}</i>
                                                                <span v-for="cty in city"
                                                                      v-if="cty.id == prop.links.listCityId">@{{ cty.mctyDescription }}</span>
                                                                    <span v-if="prop.listStreetName">@{{ prop.listStreetName | truncate(35, '...') }}</span>
                                                                    <span v-else> - </span>
                                                                </div>
                                                            </div>
                                                            </div>
                                                       </div>            
                                                    </div>
                                                 </div>
                                                  </a>
                                             </div>
                                              
                                              <div v-if="error">  
                                                <div class="hidden" >
                                                    <p style="font-size:10px;" v-if="totalrecord">Total Properties @{{ totalrecord }}</p>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                                                            <nav id="nav-below" class="site-navigation paging-navigation navbar">
                                                        <div class="nav-previous">
                                                            <a href="#top" type="text"  v-on:click="prevProperty">PREV PAGE</a>
                                                        </div> 
                                                                

                                                                <ul class="pagination pagination-lg" v-if="total">
                                                                    
                                                                    <li><a>Page  @{{ paginate }} To @{{ total | ceil }}</a> </li>
                                                                    
                                                                </ul>
                                                                <div class="nav-next">
                                                                <a  href="#top" v-on:click="getProperty" id="sub_menu">NEXT PAGE</a>
                                                               </div>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                              <div v-else-if="status">
                                                     <div class="hidden">
                                                    <p style="font-size:10px;" v-if="totalrecord">Total Properties @{{ totalrecord }}</p>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                                                            <nav id="nav-below" class="site-navigation paging-navigation navbar">
                                                        <div class="nav-previous">
                                                            <a href="#top" type="text"  v-on:click="prevProperty">PREV PAGE</a>
                                                        </div> 
                                                                

                                                                <ul class="pagination pagination-lg" v-if="total">
                                                                    
                                                                    <li><a>Page  @{{ paginate }} To @{{ total | ceil }}</a> </li>
                                                                    
                                                                </ul>
                                                                <div class="nav-next">
                                                                <a  href="#top" v-on:click="getProperty" id="sub_menu">NEXT PAGE</a>
                                                               </div>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                    </div>
                                              </div>
                                              <div v-else> 
                                                    <p style="font-size:10px;" v-if="totalrecord">Total Properties @{{ totalrecord }}</p>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                                                            <nav id="nav-below" class="site-navigation paging-navigation navbar">
                                                        <div class="nav-previous">
                                                            <a href="#top" type="text"  v-on:click="prevProperty">PREV PAGE</a>
                                                        </div> 
                                                                

                                                                <ul class="pagination pagination-lg" v-if="total">
                                                                    
                                                                    <li><a>Page  @{{ paginate }} To @{{ total | ceil }}</a> </li>
                                                                    
                                                                </ul>
                                                                <div class="nav-next">
                                                                <a  href="#top" v-on:click="getProperty" id="sub_menu">NEXT PAGE</a>
                                                               </div>
                                                            </nav>
                                                        </div>
                                                    </div>

                                              </div>
                                    {{--@endfor--}}
                                        </div>
                                    </div><!-- end container -->
                                </div>    <!-- end wide-2 -->


                            </div>    <!-- content -->
                        </div>


                        <div id="tab2" ref="tab" class="tab">
                            <div class="col-xs-12 content_2">
                                <div class="col-md-12" >
                                    <!-- Range slider -->


                                    <div class="wide-2" id="listing-property">

                                      
                                            
                                            <div class="row m0">
                                                <div class="container-404" v-if="error">
                                                    <div class="container">
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="text-404" style="text-align: center">
                                                                <div class="col-md-12 col-sm-12 no-page" style="text-align:center;">
                                                                    <span class="sorry">Oops.. Your search result not found !</span>
                                                                    <p style="text-align: center"> please search again using diffrent keywords.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                <div class="col-md-12">
                                                <div class="row">
                                                <div v-for="prop in propertys" >
                                                
                                                <div class="col-md-3"  >
                                                   <a :href="'property/' + prop.listUrl">
                                                    <div class="house-card buy">
                                                     
                                                       <div v-if="prop.links.listFile">
                                                            <div class="image-wrap"
                                                                 :style="{ backgroundImage: 'url(' + imageUri + fil.filePreview + ')'}"
                                                                 v-for="fil in file"
                                                                 v-if="fil.id == prop.links.listFile[0]"
                                                            >
                                                                <label :class="[prop.links.listListingCategoryId == 1 ? 'buy' : 'rent' ]">
                                                                    For @{{ prop.links.listListingCategoryId == 1 ? 'Sale' : 'Rent'   }}
                                                                </label>
                                                            </div>

                                                        </div>
                                                          <div class="image-wrap" style="background-image: url('/images/Not_available.jpg');width: 100%;height: 50%;"  v-else>
                                                             {{-- <img src="{{ asset('asset('images/Not_available.png') }}" width="100%" height="170px"> --}}
                                                        </div>
                                                  
                                                   
                                                        <div class="content-wrap">
                                                          
                                                                <div class="title">
                                                                    @{{ prop.listTitle }}
                                                                </div>
                                                          

                                                            <div class="price">
                                                                @{{ prop.listListingPrice ? formatRp(prop.listListingPrice) : '' }}
                                                            </div>
                                                            <div v-if="prop.listDescription">
                                                            <div class="desc">
                                                                @{{ prop.listDescription | truncate(45, '...') }}
                                                            </div>
                                                            </div>
                                                            <div v-else>
                                                            <div class="desc">
                                                                -
                                                            </div>
                                                            </div>
                                                            <div class="amenities">
                                                                <div class="row">
                                                                <div class="col-md-6 col-sm-3 col-xs-3 cat-img">
                                                                    <img src="/assets/img/bedroom.png" alt="" >   
                                                                    <p style="font-size: 10px;color:#000;" class="info-line" v-if="prop.listBedroom">@{{ prop.listBedroom }} </p>
                                                                    <p style="font-size: 10px;color:#000;" class="info-line" v-else> - </p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-3 col-xs-3 cat-img">
                                                                    <img src="/assets/img/bathroom.png" alt="">
                                                                    <p style="font-size: 10px;color:#000;" class="info-line" v-if="prop.listBathroom">@{{ prop.listBathroom }}</p>
                                                                     <p style="font-size: 10px;color:#000;" class="info-line" v-else> - </p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-3 col-xs-3 cat-img">
                                                                    <img src="/assets/img/map1.png" alt="">
                                                                    <p class="info-line" style="font-size: 10px;;color:#000;" v-if="prop.listLandSize">@{{ prop.listLandSize }} m2</p>
                                                                     <p style="font-size: 10px;color:#000;" class="info-line" v-else> - m2</p>
                                                                </div>
                                                                <div class="col-md-6 col-sm-3 col-xs-3 cat-img">
                                                                    <img src="/assets/img/square.png" alt="">
                                                                    <p class="info-line" style="font-size: 10px;color:#000;" v-if="prop.listLandSize">@{{ prop.listBuildingSize }} m2</p>
                                                                    <p style="font-size: 10px;color:#000;" class="info-line" v-else> - m2</p>
                                                                </div>
                                                                </div>
                                                             
                                                            </div>

                                                            <div class="location row m0">
                                                                <div class="col-md-12" style="padding-left:0px;">
                                                                <i class="fa fa-map-marker"></i>
                                                                <span v-for="cty in cityGrid"
                                                                      v-if="cty.id == prop.links.listCityId" >@{{ cty.mctyDescription | truncate(33, '...')}}</span>
                                                                    <p v-if="prop.listStreetName" style="font-size:10px;color:#000;" >@{{ prop.listStreetName | truncate(33, '...')}}</p>
                                                                    <p style="font-size:10px;color:#000;" v-else> N </p>                                                              
                                                                </div>
                                                            </div>
                                                             <div class="col-xs-12">

                                                        </div>
                                                        
                                                        </div>
                                                           </div>
                                                           <br>
                                                           
                                                    </div>
                                                    </a>
                                                    </div>
                                                        </div>
                                            <p style="font-size:10px;" v-if="totalrecord">Total Properties @{{ totalrecord }}</p>
                                                    <div class="col-xs-12">
                                                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                                                            <nav id="nav-below" class="site-navigation paging-navigation navbar">
                                                        <div class="nav-previous">
                                                            <a href="#" type="text"  v-on:click="prevProperty">PREV PAGE</a>
                                                        </div> 
                                                                

                                                                <ul class="pagination pagination-lg" v-if="total">
                                                                    
                                                                    <li><a>Page  @{{ paginate }} To @{{ total | ceil }}</a> </li>
                                                                    
                                                                </ul>
                                                                <div class="nav-next">
                                                                <a href="#" type="text"  v-on:click="getProperty" style="">NEXT PAGE</a>
                                                               </div>
                                                            </nav>
                                                        </div>
                                                    </div>
                                               
                                            </div>
                                            <!-- Pagination -->
                                                  
                                        </div><!-- end container -->
                                   
                                    </div>    <!-- end wide-2 -->


                                </div>


                            </div>

                        </div><!-- tab-content -->
                    </div><!-- tabs -->
                </div>
            </div>
        </div><!-- end Page Content -->

        <!-- Start Footer -->
        <!-- End Footer -->
    </div>
</div>
<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('assets/js/share-popup.js') }}"></script> -->
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('js/vue-filter.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vuejs-paginator/2.0.0/vuejs-paginator.js"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/lodash.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('assets/js/infobox.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.dependClass-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/draggable-0.1.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/jquery.slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/js/location.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('assets/js/jshashtable-2.1_src.js') }}"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBt6Pwo89NTFbXWEtrcEsL14M6af-gSjfQ"></script>
<script src="https://googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"></script>
<script type="text/javascript" src="{{ asset('assets/js/markerwithlabel_packed.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/share-popup.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/vue-sharing.js')}}"></script>
<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
<script>
  $(document).ready(function ($) {
    $('.customer.share').on("click", function(e) {
      $(this).customerPopup(e);
    });
  });
</script>
<script type="text/javascript">
    $(".tabs-wrap ul li a").on("click", function () {
        var href = $(this).attr("href");
        $(".tabs-wrap ul li a").removeClass("active");
        $(this).addClass("active");

        $(".search-wrap a").addClass("hideit");
        $(href).removeClass("hideit");
        
    });
</script>
<script type="text/javascript">
    $(".tabs-wrap ul li a").on("click", function () {
        var href = $(this).attr("href");
        $(".tabs-wrap ul li a").removeClass("active");
        $(this).addClass("active");

        $(".search-wrap a").addClass("hideit");
        $(href).removeClass("hideit");
        
    });
</script>
<script type="text/javascript">

 $(document).ready(function() {

    $('a[href=#top]').click(function(){
        $('#scroll-properties').animate({scrollTop:0}, 'slow');
        return false;
    });

});

</script>
<script>    
    var markerHov = [];
    var language = "{{ Session::get('language') }}";
    Vue.use(SocialSharing); 
    Vue.config.devtools = true;
    var app = new Vue({
        el: '#page-content-search',
        data: {
            error: false,
            coordinat: [],
            classCategory: true,
            status: true,
            sortKey: '',
            reverse: false,
            language: window.sessionStorage.setItem('language', language),
            priceRange: [
                1000000, 10000000, 20000000, 30000000, 40000000, 50000000, 80000000, 100000000, 150000000, 200000000, 250000000, 300000000, 350000000, 400000000, 450000000, 500000000,
                600000000, 700000000, 800000000, 900000000, 1000000000, 2000000000, 3000000000, 5000000000, 8000000000, 10000000000, 100000000000
            ],

            listApi: {
                filterByCity: '&filter[mctyDescription]=',
                filterByTitle: '&filter[listTitle]=',
                filterByBedroom: '&filter[listBedroom]=',
                filterByMinPrice: '&filter[listListingPrice][>]=',
                filterByMaxPrice: '&filter[listListingPrice][<]=',
                getListingCategory: 'ListingCategory',
                filterByCategoryId: '&filter[listListingCategoryId]=',
                getListingFacility: 'facility',
                filterByFacility: '&filter[listFacility][in]=',
                filterByBuildingSize: '&filter[listBuildingSize][>%3D]=',
                orderByNewestDate : 'listing?sort=-listPublishDate',
                filterByPropertyType : '&filter[listPropertyTypeId]=',
                fetchPropertyType : 'propertytype',
                filterByLandSize : '&filter[listLandSize][>%3D]=',
                filterByBath : '&filter[listBathroom]=',
                filterBySearch : '&filter[search]='

            },
            counter:1,
            prev_page : 1,
            next_page: 1,
            page_number :[],
            next_page : [],
            offset: 4,
            bath : '',
            fileGrid : [],
            cityGrid : [],
            propGrid : [],
            // propstype :'',
            propstype: [],
            landSize : '',
            elastic : '',
            minPrice: '',
            propertys: '',
            maxPrice: 100000000000,
            minmax: '',
            loading: false,
            categoryType: [],
            categoryIs: '',
            property: [],
            checkedFacility: [],
            checkedProperty: [],
            buildingSize: '',
            facility: [],
            city: [],
            file: [],
            total: 0,
            test: [],
            totalrecord: [],
            mmbs: [],
            office: [],
            paginate:[] ,
            query: '',
            title: '',
            file_error: '',
            bed: '',
            propertyType : [],
            filterCategory: '',
            baseUri: 'https://www.remax.co.id/prodigy/papi/',
            imageUri: 'https://www.remax.co.id/prodigy/papi/'
         
        },

        ready: function () {
            this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 3,
                    center: {lat: -6.21, lng: 106.85},
                    // scrollwheel : false,
                    // disableDefaultUI: true,
                });

            var myMarker = new google.maps.Marker({
                      map: this.map,
                      animation: google.maps.Animation.DROP,
                      position: {lat: -6.21, lng: 106.85}
                    });
            this.createMap();
            this.getMyLocation(this.map, myMarker);
            this.getVueItems(this.pagination.current_page);
        },

        created: function () {
            this.fetchPropertyType();
            this.fetchFacility();
            this.fetchListCategory();
            this.getValue();
            this.getCoordinat();
             this.prevProperty();
            this.getProperty();
            this.createMap();

                        this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: {lat: -6.21, lng: 106.85},
                    // scrollwheel : false,
                    // disableDefaultUI: true,
                });
            var myMarker = new google.maps.Marker({
                      map: this.map,
                      animation: google.maps.Animation.DROP,
                      position: {lat: -6.21, lng: 106.85}
                    });
            // this.createMap();
            this.getMyLocation(this.map, myMarker);
        },

        methods: {

            resetFormButton: function() {
                var vm = this;
              
                vm.query = '',
                vm.bed = '',
                vm.minPrice = '',
                vm.maxPrice = '',
              
                vm.filterCategory = '',
                vm.buildingSize = '',
                vm.checkedFacility = [],
                vm.checkedProperty = [],
                // vm.propstype = ''
                vm.propstype = []
                
                this.createMap();
                 this.prevProperty();
                this.getProperty();
                this.propertyGrid();
            },
            resetButton : function(){
                // var vm = this;

                // vm.query = '',
                // vm.bed = '',
                // vm.minPrice = '',
                // vm.maxPrice = '',
                // vm.filterCategory = '',
                // vm.buildingSize = '',
                // vm.checkedFacility = [],
                // vm.checkedProperty
                // vm.propstype = ''

                // this.createMap();
                this.prevProperty();
                this.getProperty();
                this.propertyGrid();
                return this.resetFormButton();
            },
            resetFeature: function() {
                var vm = this;
                vm.query = ''
                vm.checkedFacility = []
                // vm.propstype = ''
                this.createMap();
                this.propertyGrid();
            },
            resetProperty: function() {
                var vm = this;
                vm.query = ''
                // vm.property = []
                // vm.propertyType = []
                // vm.propstype = []
                vm.checkedProperty = []
                vm.propertys = []
                // vm.propstype = ''
                this.createMap();
                this.propertyGrid();
            },
            resetAllSearch: function() {
                var vm = this;
                vm.query = ''
                vm.minPrice = '',
                vm.maxPrice = '',
                // vm.propertys = '',
               
                vm.propertys = [],
             
                vm.filterCategory = ''
                vm.buildingSize = '',
                vm.checkedFacility = []
                vm.checkedProperty = []
                // vm.propstype = '',
                vm.propstype = [],
                vm.landSize = ''
                vm.elastic = ''
                vm.buildingSize = ''
                vm.bed = ''
                vm.sortKey = '';
                this.getProperty();
                this.prevProperty();
                this.createMap();
                this.propertyGrid();
            },

            fetchPropertyType : function(){
                var vm = this;
 
                axios.get(vm.baseUri+ vm.listApi.fetchPropertyType)
                        .then(function(response){
                            vm.propertyType = response.data.data;
                        })
                        .catch(function(error){
                            console.log(error);
                        })

            },

            showMe : function(index){
                if(this.markerHov[index].getAnimation() != google.maps.Animation.BOUNCE){
                    this.markerHov[index].setAnimation(google.maps.Animation.BOUNCE);
                }else{
                    this.markerHov[index].setAnimation(null);
                }
            },


            getMyLocation: function(map, marker) {

                var controlDiv = document.createElement('div');

                var firstChild = document.createElement('button');
                firstChild.style.backgroundColor = '#fff';
                firstChild.style.border = 'none';
                firstChild.style.outline = 'none';
                firstChild.style.width = '28px';
                firstChild.style.height = '28px';
                firstChild.style.borderRadius = '2px';
                firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
                firstChild.style.cursor = 'pointer';
                firstChild.style.marginRight = '10px';
                firstChild.style.padding = '0';
                firstChild.title = 'Your Location';
                controlDiv.appendChild(firstChild);

                var secondChild = document.createElement('div');
                secondChild.style.margin = '5px';
                secondChild.style.width = '18px';
                secondChild.style.height = '18px';
                secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
                secondChild.style.backgroundSize = '180px 18px';
                secondChild.style.backgroundPosition = '0 0';
                secondChild.style.backgroundRepeat = 'no-repeat';
                firstChild.appendChild(secondChild);

                // google.maps.event.addListener(map, 'center_changed', function () {
                //     secondChild.style['background-position'] = '0 0';
                // });

                firstChild.addEventListener('click', function () {
                    var imgX = '0',
                   
                        animationInterval = setInterval(function () {
                            imgX = imgX === '-18' ? '0' : '-18';
                            secondChild.style['background-position'] = imgX+'px 0';
                        }, 500);
                
                    if(navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                            map.setCenter(latlng);
                            clearInterval(animationInterval);
                            secondChild.style['background-position'] = '-144px 0';
                        });
                    } else {
                        clearInterval(animationInterval);
                        secondChild.style['background-position'] = '0 0';
                    }
                });

                controlDiv.index = 1;
                map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
            },

            createMap: _.debounce(function () {
                var vm = this;
                var map;
                var maxZoomService;
                var infoWindow;
                // vm.property = [];
                vm.propertys = [];
                vm.city = [];
                vm.file = [];
                vm.paginate = [];
                vm.mmbs = [];
           
                vm.office = [];
                vm.status = true;
                var imgFile = [];


                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: {lat: -6.121435, lng: 106.774124},
                    // scrollwheel : false,
                    // disableDefaultUI: true,
                });

                google.maps.event.addListener(this.map, 'idle', function () {
                    vm.status = true;
                    var ne1 = this.getBounds().getNorthEast().lat();
                    var ne2 = this.getBounds().getNorthEast().lng();
                    var sw1 = this.getBounds().getSouthWest().lat();
                    var sw2 = this.getBounds().getSouthWest().lng();

                    setTimeout(function(){
                        listedHover();
                        markerHover();
                    }, 1000); 


                    axios.get('https://www.remax.co.id/prodigy/papi/listing?fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='+  vm.elastic)
                            .then(function (response) {
                                if (response.data.data == null) {
                                    vm.propertys = [];
                                    vm.error = false;
                                    vm.status = false;
                                } else {
                                    vm.error = false;
                                    vm.propertys = [];
                                    vm.status = false;
                                    markerHov = [];
                                    vm.propertys = response.data.data;
                                    vm.city = response.data.linked.listCityId;
                                    vm.file = response.data.linked.listFile;
                                    vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.total = response.data.status.totalRecords;
                                     if (vm.total) {
                                         vm.total = vm.total / 100 ;         
                                    }
                                    vm.totalrecord = response.status.totalRecords;
                                    vm.next_page = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    var infoWindow = new google.maps.InfoWindow(), marker, i;
                                    for (i = 0; i < vm.propertys.length; i++) {

                                        (function (index) {
                                            var x = JSON.parse(vm.propertys[i].listCoordinat);

                                            var icon = {
                                                url: "../images/new-map-marker.png", // url
                                                scaledSize: new google.maps.Size(40, 40), // scaled size
                                                origin: new google.maps.Point(0,0), // origin
                                                anchor: new google.maps.Point(0, 0) // anchor
                                            };

                                            // marker = new MarkerWithLabel({
                                            //     position: {
                                            //         lat: x.coordinate.latitude,
                                            //         lng: x.coordinate.longitude
                                            //     },
                                            //     id: vm.property[index].id ,
                                            //     map: app.map,

                                            //     labelContent: '<div class="marker-loaded" data-target="marker'+vm.property[index].id+'"><div class="map-marker"></div></div>',
                                            //     // labelContent: '<h1>Hello World</h1>',
                                            //     // labelAnchor: new google.maps.Point(10, 0),
                                            //     labelClass: "marker-style",
                                            //     icon: icon

                                            // });

                                            // var urlProp = 'property/' + vm.property[index].listUrl;

                                            // var content = '<h4 class="firstHeading" style="color:black;">' + vm.property[index].listTitle + '</h4>' +
                                            //         '<h6 style="color:#0055a5;">' + vm.formatRp(vm.property[index].listListingPrice) + '</h6>' +
                                            //         ('<a href="'  + urlProp + '">More <Detail></Detail></a>');

                                            // marker.addListener('mouseover', function () {
                                            //     infoWindow.setContent(content);
                                            //     infoWindow.open(map, this);
                                            // });

                                            // marker.addListener('mouseout', function () {
                                            //    infoWindow.close();
                                            // });

                                            console.log(x.coordinate.latitude, x.coordinate.longitude)
                                            marker = new MarkerWithLabel({
                                                position: {
                                                    lat: x.coordinate.latitude,
                                                    lng: x.coordinate.longitude
                                                },
                                                id: vm.propertys[index].id ,
                                                map: app.map,
                                               labelContent: '<div class="marker-loaded" data-target="marker'+vm.propertys[index].id+'"><div class="map-marker" ><img src="../../images/new-map-marker.png"></div></div>',
                                               labelAnchor: new google.maps.Point(50, 0),
                                               // labelClass: "marker-style",
                                               labelAnchor: new google.maps.Point(0, 0),
                                                icon: icon
                                            });

                                            var urlProp = 'property/' + vm.propertys[index].listUrl;

                                            var circle = new google.maps.Circle({
                                                map: app.map,
                                                // radius: x.radius,
                                                // strokeColor: '#3e3e3e',
                                                // strokeWeight: 0.5,
                                                // fillColor: '#3e3e3e',
                                                // fillOpacity: 0.12
                                            });

                                            circle.bindTo('center',marker,'position');


                                            var content = '<h4 class="firstHeading" style="color:black;">' + vm.propertys[index].listTitle + '</h4>' +
                                                    '<h6 style="color:#0055a5;">' + vm.formatRp(vm.propertys[index].listListingPrice) + '</h6>' +
                                                    ('<a href="'  + urlProp + '">More <Detail></Detail></a>');



                                            marker.addListener('mouseover', function () {
                                                infoWindow.setContent(content);
                                                infoWindow.open(map, this);

                                            });

                                            marker.addListener('click', function() {
                                                infoWindow.setContent(content);
                                                infoWindow.open(map, this);
                                            })

                                            marker.addListener('mousemove', function() {
                                                // infoWindow.close();
                                                infoWindow.setContent(content);
                                                infoWindow.open(map, this);
                                            });

                                            // marker.addListener('mouseout', function() {
                                            //     infoWindow.close();
                                            // })

//                                            var locations = [
//                                                {
//                                                    lat: x.coordinate.latitude,
//                                                    lng: x.coordinate.longitude
//                                                }
//                                            ];
//                                            var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
//                                            var markers = locations.map(function(location, i) {
//                                                return new google.maps.Marker({
//                                                    position: location,
//                                                    label: labels[i % labels.length]
//                                                });
//                                            });
//
//                                            var map = new google.maps.Map(document.getElementById('map'), {
//                                                center: {
//                                                    lat: -0.789275,
//                                                    lng: 113.921327
//                                                }
//                                            })
//                                            new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'})


                                        })(i);
                                    }
                                }
                            })


                            .catch(function (error) {
                                console.log(error);
                            });

                });

             google.maps.event.addListener(this.map, 'idle', function () {

             });

            }, 500),




            locateMap: _.debounce(function () {
                var geocoder = new google.maps.Geocoder();
                var vm = this;
                vm.status = true;
                vm.propertys = [];
                vm.error = '';
                vm.city = [];
                vm.file = [];
                var markers = [];

                if(vm.elastic == ''){
                    vm.elastic = 'jakarta'
                }

                geocoder.geocode({address: vm.elastic}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {

                        vm.map.setCenter(results[0].geometry.location);
                            // this.advanceSearch();
                            axios.get('https://www.remax.co.id/prodigy/papi/listing?fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='+  vm.elastic )
                                    .then(function (response) {
                                        if (response.data.data == null) {
                                           
                                        } else {
                                                                           
                                        }

                                        var bounds = new google.maps.LatLngBounds();

                                        var newMarkers = [];
                                        var infoWindow = new google.maps.InfoWindow(), marker, i;
                                        for (i = 0; i < vm.property.length; i++) {

                                        
                                        
                                            var x = JSON.parse(vm.propertys[i].listCoordinat);


                                            if (x) {
                                                marker = new google.maps.Marker({
                                                    position: {
                                                        lat: x.coordinat.latitude,
                                                        lng: x.coordinat.longitude
                                                    },
                                                    map: app.map
                                                });

                                                markers.push(marker);
                                                markers[i].setMap(null);
                                                allert('')
                                                bounds.extend({
                                                            
                                                        lat: x.coordinat.latitude,
                                                        lng: x.coordinat.langitude
                                                    }
                                                );
                                                app.map.fitBounds(bounds);
                                            }
                                        }
                               
                             
                                    })
                                .catch(function (error) {
                                    console.log(error);
                                    vm.status = false;
                                    vm.error = 'Property not found , try with different keyword !';
                                });
                    }

                })
            }, 500),
    
            getProperty :function(){

                var vm = this;
             
                    
                        vm.paginate++
                    
             
                  axios.get( vm.baseUri + 'listing?fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='+  vm.elastic )
                .then(function(response) {
                    console.log(response)
                    if(response.data.data == null){
                        vm.propertys = [];
                        vm.error = true;
                        vm.status = false;
                    }else{
                        vm.error = false;
                        vm.propertys = [];
                        vm.status = false;
                        markerHov = [];
                        vm.propGrid = response.data.data;
                        vm.propertys = response.data.data;
                        vm.city = response.data.linked.listCityId;
                        vm.file = response.data.linked.listFile;
                        vm.office = response.data.linked.listOfficeId;
                        vm.paginate = response.data.status.page;
                        vm.total = response.data.status.totalRecords;
                         if (vm.total) {
                             vm.total = vm.total / 100 ;         
                        }
                        vm.totalrecord = response.data.status.totalRecords;
                        vm.next_page = response.data.status.page;
                        vm.mmbs = response.data.linked.listMmbsId;
                    }




                })
           
                .catch(function (error) {
                    console.log(error);
                    vm.status = false;
                    vm.error = 'Property not found, try with different keyword !';
                });
                


            },
        prevProperty :function(){

                var vm = this;
                
                    vm.paginate--
                    if(vm.paginate <= 1){
                        vm.paginate = 0;
                    }


                  axios.get( vm.baseUri + 'listing?fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='+  vm.elastic)
                .then(function(response) {
                    console.log(response)
                    if(response.data.data == null){
                        vm.propertys = [];
                        vm.error = true;
                        vm.status = false;
                    }else{
                        vm.error = false;
                        vm.propertys = [];
                        vm.status = false;
                        markerHov = [];
                        vm.propGrid = response.data.data;
                        vm.propertys = response.data.data;
                        vm.city = response.data.linked.listCityId;
                        vm.file = response.data.linked.listFile;
                        vm.office = response.data.linked.listOfficeId;
                        vm.paginate = response.data.status.page;
                        vm.total = response.data.status.totalRecords;
                         if (vm.total) {
                             vm.total = vm.total / 100 ;         
                        }
                        vm.totalrecord = response.status.totalRecords;
                        vm.next_page = response.data.status.page;
                        vm.mmbs = response.data.linked.listMmbsId;
                    }




                })
           
                .catch(function (error) {
                    console.log(error);
                    vm.status = false;
                    vm.error = 'Property not found, try with different keyword !';
                });
                


            },

            multipleCall : function(){
                var vm = this;
              vm.advanceSearch();
              vm.propertyGrid();
            },

                advanceSearch: _.debounce(function () {
                var vm = this;
                vm.propertys = [];
                vm.status = true;
                vm.error = '';
                vm.file = [];
                vm.city = [];

                if(vm.elastic == ''){
                    vm.listApi.filterBySearch = ''
                }else{
                    vm.listApi.filterBySearch = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='
                    
                }


                if(vm.bath == ''){
                    vm.listApi.filterByBath = ''
                }else{
                    vm.listApi.filterByBath = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listBathroom]='
              
                }

                if(vm.landSize ==''){
                    vm.listApi.filterByLandSize = ''
                }else{
                    vm.listApi.filterByLandSize = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listLandSize][>%3D]='
                   
                }

                if (vm.filterCategory == '') {
                    vm.listApi.filterByCategoryId = ''
                }else{
                    vm.listApi.filterByCategoryId = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+ '&filter[listListingCategoryId]='
                    
                }

                if(vm.query == ''){
                    vm.listApi.filterByCity = ''
                }else{
                    vm.listApi.filterByCity = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[mctyDescription]=%27%25'+ vm.query +'%25%27'
                   
                }

                if(vm.checkedProperty == ''){
                  vm.listApi.filterByPropertyType = ''
                }else{
                  vm.listApi.filterByPropertyType = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listPropertyTypeId][in]='
                  
                }

                if(vm.bed == ''){
                    vm.listApi.filterByBedroom = ''
                }else if(vm.bed == 5){
                    vm.listApi.filterByBedroom = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listBedroom][>]='
                    
                }else{
                    vm.listApi.filterByBedroom = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listBedroom]='
                
                    }

                if (vm.minPrice == '') {
                    vm.listApi.filterByMinPrice = ''
                }else{
                    vm.listApi.filterByMinPrice = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listListingPrice][gte]='
                   
                }

                if (vm.maxPrice == '') {
                    vm.listApi.filterByMaxPrice = ''
                }else{
                    vm.listApi.filterByMaxPrice = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listListingPrice][lte]='
                 
                }

                if (vm.buildingSize == '') {
                    vm.listApi.filterByBuildingSize = ''
                }else{
                    vm.listApi.filterByBuildingSize = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listBuildingSize][>%3D]='
                   
                }

                if(vm.checkedFacility == ''){
                    vm.listApi.filterByFacility = ''
                }else{
                    vm.listApi.filterByFacility = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listFacility][in]='
                    
                }


                axios.get(vm.baseUri+ vm.listApi.orderByNewestDate
                                    + vm.listApi.filterByCity
                                    + vm.listApi.filterByBedroom + vm.bed
                                    + vm.listApi.filterByMinPrice + vm.minPrice
                                    + vm.listApi.filterByMaxPrice + vm.maxPrice
                                    + vm.listApi.filterByminmax + vm.minmax
                                    + vm.listApi.filterByCategoryId + vm.filterCategory
                                    + vm.listApi.filterByBuildingSize + vm.buildingSize
                                    + vm.listApi.filterByFacility + vm.checkedFacility
                                    + vm.listApi.filterByPropertyType + vm.checkedProperty
                                    + vm.listApi.filterByLandSize + vm.landSize
                                    + vm.listApi.filterBySearch + vm.elastic)

                        .then(function (response) {
                            if (response.data.data == null) {
                                vm.error = true;
                                 vm.status = false;
                                vm.propertys = [];
                            } else {
                                
                                vm.error = false;
                                vm.status = false;
                                vm.propertys = response.data.data;
                                vm.office = response.data.linked.listOfficeId;
                                vm.mmbs = response.data.linked.listMmbsId;
                                vm.file = response.data.linked.listFile;
                                vm.city = response.data.linked.listCityId;
                            }
                        })

                        .catch(function (error) {
                            console.log(error)
                        });
            }, 500),

            propertyGrid : function(){
                var vm = this;
                vm.propGrid = [];
                vm.status = true;
                vm.error = '';
                vm.fileGrid = [];
                vm.cityGrid = [];

                if(vm.elastic == ''){
                    vm.listApi.filterBySearch = ''
                }else{
                    vm.listApi.filterBySearch = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[search]='
                }

                if(vm.landSize ==''){
                    vm.listApi.filterByLandSize = ''
                }else{
                    vm.listApi.filterByLandSize = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' +'&filter[listLandSize][>%3D]='
                }


                if(vm.bath == ''){
                    vm.listApi.filterByBath = ''
                }else{
                    vm.listApi.filterByBath = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listBathroom]='
                }

                if (vm.filterCategory == '') {
                    vm.listApi.filterByCategoryId = ''
                }else{
                    vm.listApi.filterByCategoryId ='&fields=1&pageNumber=' + vm.paginate + '&pageSize=100' + '&filter[listListingCategoryId]='
                }

                if(vm.query == ''){
                    vm.listApi.filterByCity = ''
                }else{
                    vm.listApi.filterByCity = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[mctyDescription]=%27%25'+ vm.query +'%25%27'
                }

                if(vm.checkedProperty == ''){
                    vm.listApi.filterByPropertyType = ''
                }else{
                    vm.listApi.filterByPropertyType ='&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+ '&filter[listPropertyTypeId][in]='
                }

                if(vm.bed == ''){
                    vm.listApi.filterByBedroom = ''
                }else if(vm.bed == 5){
                    vm.listApi.filterByBedroom = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[listBedroom][>]='
                }else{
                    vm.listApi.filterByBedroom = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[listBedroom]='
                }

                if (vm.minPrice == '') {
                    vm.listApi.filterByMinPrice = ''
                }else{
                    vm.listApi.filterByMinPrice = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[listListingPrice][>]='
                }

                if (vm.maxPrice == '') {
                    vm.listApi.filterByMaxPrice = ''
                }else{
                    vm.listApi.filterByMaxPrice = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[listListingPrice][<]='
                }

                if (vm.buildingSize == '') {
                    vm.listApi.filterByBuildingSize = ''
                }else{
                    vm.listApi.filterByBuildingSize ='&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+ '&filter[listBuildingSize][>%3D]='
                }

                if(vm.checkedFacility == ''){
                    vm.listApi.filterByFacility = ''
                }else{
                    vm.listApi.filterByFacility = '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100'+'&filter[listFacility][in]='
                }


                axios.get(vm.baseUri + vm.listApi.orderByNewestDate
                                + vm.listApi.filterByCity
                                + vm.listApi.filterByBedroom + vm.bed
                                + vm.listApi.filterByMinPrice + vm.minPrice
                                + vm.listApi.filterByMaxPrice + vm.maxPrice
                                + vm.listApi.filterByCategoryId + vm.filterCategory
                                + vm.listApi.filterByBuildingSize + vm.buildingSize
                                + vm.listApi.filterByFacility + vm.checkedFacility
                                + vm.listApi.filterByPropertyType + vm.checkedProperty
                                + vm.listApi.filterByLandSize + vm.landSize
                                + vm.listApi.filterBySearch + vm.elastic)
                        .then(function (response) {
                            if (response.data.data == null) {
                                vm.propGrid = [];
                                vm.error = true;
                                vm.status = false;

                            } else {
                                vm.status = false;
                                vm.propGrid = response.data.data;
                                vm.fileGrid = response.data.linked.listFile;
                                vm.cityGrid = response.data.linked.listCityId;
                                vm.office = response.data.linked.listOfficeId;
                                vm.paginate = response.data.status.page;
                                vm.mmbs = response.data.linked.listMmbsId;
                                vm.total = response.data.status.totalRecords;
                                if (vm.total) {
                                        vm.total = vm.total / 100 ;         
                                        }
                            vm.totalrecord = response.data.status.totalRecords;
                            }
                        })

                        .catch(function (error) {
                            console.log(error)

                        });

            },
            sortByValue: _.debounce(function (sortKey) {
                var vm = this;
                vm.status = true;
                vm.propGrid = [];
                vm.propertys = [];
                vm.error = '';
                sortKey = vm.sortKey;
                vm.file = [];
                vm.city = [];

                switch (sortKey) {

                      case 'reset':
                        axios.get(vm.baseUri + 'listing' + '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                    vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                    case 'cheapest':
                        axios.get(vm.baseUri + 'listing' + '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=%2BlistListingPrice')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                    vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                    case 'expensive':
                        axios.get(vm.baseUri + 'listing' + '&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=-listListingPrice')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                    case 'newest':
                        axios.get(vm.baseUri + 'listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=-listPublishDate')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                     case 'oldest':
                        axios.get(vm.baseUri + 'Listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=listPublishDate')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                        case 'smallLandarea':
                        axios.get(vm.baseUri + 'Listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=%2BlistLandSize')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                         case 'largeLandarea':
                        axios.get(vm.baseUri + 'Listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=-listLandSize')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                         case 'smallBuildingarea':
                        axios.get(vm.baseUri + 'Listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=%2BlistBuildingSize')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                         case 'largestBuildingarea':
                        axios.get(vm.baseUri + 'Listing'+'&fields=1&pageNumber=' + vm.paginate + '&pageSize=100?sort=-listBuildingSize')
                                .then(function (response) {
                                    vm.status = false;
                                    vm.propGrid = response.data.data;
                                    vm.propertys = response.data.data;
                                    vm.file = response.data.linked.listFile;
                                    vm.fileGrid = response.data.linked.listFile;
                                    vm.city = response.data.linked.listCityId;
                                    vm.cityGrid = response.data.linked.listCityId;
                                     vm.office = response.data.linked.listOfficeId;
                                    vm.paginate = response.data.status.page;
                                    vm.mmbs = response.data.linked.listMmbsId;
                                    vm.total = response.data.status.totalRecords;
                                    if (vm.total) {
                                            vm.total = vm.total / 100 ;         
                                            }
                                    vm.totalrecord = response.data.status.totalRecords;

                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        break;
                        //  case 'lowpriceMeters':
                        // axios.get(vm.baseUri + 'Listing?sort=listPublishDate')
                        //         .then(function (response) {
                        //             vm.status = false;
                        //             vm.propGrid = response.data.data;
                        //             vm.property = response.data.data;
                        //             vm.file = response.data.linked.listFile;
                        //             vm.fileGrid = response.data.linked.listFile;
                        //             vm.city = response.data.linked.listCityId;
                        //             vm.cityGrid = response.data.linked.listCityId;
                        //         })
                        //         .catch(function (error) {
                        //             console.log(error);
                        //         });
                        // break;
                        // case 'highpriceMeters':
                        // axios.get(vm.baseUri + 'Listing?sort=listPublishDate')
                        //         .then(function (response) {
                        //             vm.status = false;
                        //             vm.propGrid = response.data.data;
                        //             vm.property = response.data.data;
                        //             vm.file = response.data.linked.listFile;
                        //             vm.fileGrid = response.data.linked.listFile;
                        //             vm.city = response.data.linked.listCityId;
                        //             vm.cityGrid = response.data.linked.listCityId;
                        //         })
                        //         .catch(function (error) {
                        //             console.log(error);
                        //         });
                        // break;
                    case '':
                        this.locateMap();
                        break;
                }

            }, 200),

            getCoordinat: function (key) {
                return JSON.parse((key || null ));
            },

            getValue: function () {
                var vm = this;
                var getQuery = {};


                if (location.search !== "") {

                    getQuery = location.search.substr(1).split("=");

                    vm.elastic = getQuery[1].replace('+', ' ');

                    if (getQuery[0] == 'buySearch') {
                        vm.filterCategory = 1;
                    } else if(getQuery[0] != 'buySearch' && getQuery[0] != 'rentSearch'){
                       vm.elastic = ''
                    }else{
                        vm.filterCategory = 2;
                    }

                    this.locateMap();


                } else {
                    vm.elastic = '';
                }

            },


            fetchListCategory: function () {
                var vm = this;


                axios.get(vm.baseUri + 'ListingCategory')
                        .then(function (response) {
                            vm.categoryType = response.data.data;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
            }
            ,

            fetchFacility: function () {
                var vm = this;

                axios.get(vm.baseUri + 'facility')
                        .then(function (response) {
                            vm.facility = response.data.data;

                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                axios.get(vm.baseUri + 'facility')
                        .then(function (response) {
                            vm.facility = response.data.data;

                        })
                        .catch(function (error) {
                            console.log(error);
                        });

            },

            formatRp: function (value) {
                var x = value;
                if (x >= 1000000000) {
                    x = Math.round((x / 1000000000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' M';
                } else {
                    x = Math.round((x / 1000000)).toFixed(0);
                    return x = 'Rp.' + ' ' + x.toString().replace(/\./g, ',') + ' Jt';
                }
            }

        }
    });
</script>

<script>
// var value = 1000000000
// var x =     1290500000
// console.log(x.toFixed(2))
</script>

<!--[if gt IE 8]>->
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>
<![endif]-->
<script>
    $(".image-wrap").each(function () {
        $(this).lazyload({
            effect: "fadeIn",
            thumbnail: true,
            animateThumb: true,
            showThumbByDefault: true
        });
    });

    setTimeout(function(){
        listedHover();

        markerHover();

    }, 3000);

    function listedHover(){
        // House Box\
        $(".markers").on("mouseenter", function(){
            var id = $(this).attr("id");

            $(".markers").css("opacity", "0.5");
            $(this).css("opacity", "1");

            $(".marker-loaded").find(".map-marker");
            $(".marker-loaded[data-target="+id+"]").addClass("marker-active");
        });
        $(".markers").on("mouseleave", function(){
            var id = $(this).attr("id");

            $(".markers").css("opacity", "1");
            $(".marker-loaded").removeClass("marker-active");
        });
    }

    function markerHover(){
        // Map Marker
        $(".marker-loaded").on("mouseenter", function(){
            var id = $(this).attr("data-target");

            $(".markers").css("opacity", "0.5");
            $(".markers[id='"+id+"']").css("opacity", "1");

            // $(".marker-loaded").find(".map-marker").css("background-color", "#fff");
            $(this).addClass("marker-active");
        });
        $(".marker-loaded").on("mouseleave", function(){
            var id = $(this).attr("data-target");

            $(".markers").css("opacity", "1");
            $(this).removeClass("marker-active");
        });
    }


</script>

<script>
function popupShare(url) {
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var intWidth = '900';
    var intHeight = '500';

    var left = ((width / 2) - (intWidth / 2)) + dualScreenLeft;
    var top = ((height / 2) - (intHeight / 2)) + dualScreenTop;
    var strTitle = 'Social Share',
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=1' + ',top=' + top + ',left=' + left;

    window.open(url, strTitle, strParam).focus();


}
</script>


</body>
</html>

