<!DOCTYPE html>

<html lang="en-US">
<head>

    <title>Property Page</title>

    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if($body->data->listUrl == null)
    @else
    <meta property="og:url" content="{{ route('property.show',$body->data->listUrl) }}"/>
    @endif
    <meta property="og:type" content="website"/>
    @if($body->data->listTitle == null)
    @else
    <meta property="og:title" content="{{ $body->data->listTitle }}"/>
    @endif

    @if($body->data->listDescription == null)
    @else
    <meta property="og:description" content="{{ $body->data->listDescription }}"/>
    @endif
    

    @if($body->data->links->listFile == null)
    @else
    <meta property="og:image" content="{{ $body->linked->listFile[0]->filePreview }}"/> 
    @endif
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,800" rel="stylesheet">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel='stylesheet' type='text/css' href='http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css'/>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/customsas.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />


    <style>
.badge {
  font-size: 18.025px;
  font-weight: bold;
  white-space: nowrap;
  color: #ffffff;
  border-radius: 0px;
  box-shadow: 0px 4px 14px 1px rgba(0, 0, 0, 0.0);
  padding:8px 25px;
  background-color: #999999;
}

.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}

.badge-sell {
    background-color: #e21a22;
}

.badge-rent {
    background-color: #3a7de3;
}
.badge-notfound {
    background-color: #ff9800;
}
.detail-property {
    background-color: #FFFFFF;
}

/*.center {
    text-align: center;
}*/
 #media-accordion  .panel-heading{
        margin:0;
    }
  .image-news .caption-news {
      vertical-align: middle;
      text-align: center;
      top: 50%;
  }

  .caption-news span.border {
      background-color: #E21A22;
      opacity: 0.7;
      color: white;
      padding: 18px;
      letter-spacing: 5px;
      font-size: 13px;
      border: 2px;
      border-radius: 10px
  }

  .panel-group .panel {
    border-radius: 0;
    box-shadow: none;
    border-color: #EEEEEE;
  }

  .panel-default > .panel-heading {
    padding: 0;
    border-radius: 0;
    color: #212121;
    background-color: #FAFAFA;
    border-color: #EEEEEE;
  }

  .panel-title {
    font-size: 14px;
  }

  .panel-title > a {
    display: block;
    padding: 30px;
    text-decoration: none;
  }

  .more-less {
    float: right;
    color: #212121;
  }

  .panel-default > .panel-heading + .panel-collapse > .panel-body {
    border-top-color: #EEEEEE;
  }

  li:before
{
    /*content: '✔';*/
    margin-left: -1em;
    margin-right: .100em;
}

ul
{
   padding-left: 20px;
   text-indent: 2px;
   list-style: none;
   list-style-position: outside;
}

.section3{
    width: 100%;
    background: u
    background: url('../img/curriculam_bg.png');
    background-size: cover;
    padding-top: 50px;
    padding-bottom: 50px;
    margin-bottom: 1px;
    display: table;
}

.section3_heading{
    background: #d4eae4;
    color:#003057;
    text-align: center;
    padding-bottom: 20px;
    padding-top: 20px;
    transition: 1s ease;
}
.section3_heading:hover{
    background: #003057;
    color:#fff;
    transition: 1s ease;
}

.section3_heading:hover + .section3_body{
    border-bottom: 5px solid #003057;
    transition: 1s ease;
}


.section3_heading h2{
    font-weight: 900;
    text-transform: uppercase;
    margin: 0px;
}

.section3_body{
    background: #fff;
    padding: 30px 50px 20px 50px;
    border-bottom: 5px solid #d4eae4;
    min-height: 200px;
    transition: 1s ease;
}

.section3_body li{
    margin-bottom: 10px;
}

.note {
    padding-left: 20px;
    font-size: 12px;
    font-style: italic;
}

.share-news ul {
    padding-left: 34px;
}
@media screen and (max-width: 1024px) {
.share-news ul {
    padding-left: 18px;
}
}
    </style>
</head>

<body class="page-property" id="page-top">

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1243959699053772',
            xfbml: true,
            version: 'v2.8'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->
    <!-- Page Content -->
    <div id="page-property-content">
    <div class="wide_container_3 carousel-full-width">
        <div class="tabs">
          <div class="tab-content">
            <div id="tab1" class="tab active">
              <!-- Owl carousel -->
              <div id="owl-demo-2" class="owl-carousel owl-theme owl-prop-carousel">
                @if($body->data->links->listFile == null)
                <div class="col-md-4">
                  <div class="owl-prop-item" > 
                      <div class="owl-prop-back"
                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                        <div class="owl-prop-img">
                            <img src="{{ asset('images/Not_available.jpg') }}"
                                 alt="" class="img-responsive">
                        </div> 
                    </div>
                </div>
                <div class="col-md-4">
                   <div class="owl-prop-item" > 
                      <div class="owl-prop-back"
                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                        <div class="owl-prop-img">
                            <img src="{{ asset('images/Not_available.jpg') }}"
                                 alt="" class="img-responsive">
                        </div>    
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="owl-prop-item" > 
                      <div class="owl-prop-back"
                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                        <div class="owl-prop-img">
                            <img src="{{ asset('images/Not_available.jpg') }}"
                                 alt="" class="img-responsive">
                        </div> 
                    </div>
                </div>
                @else
                    
                      @foreach($body->linked->listFile as $datas)
                         
                          @if($datas == null)
                          <div class="col-md-4">
                                  <div class="owl-prop-item" > 

                                      <div class="owl-prop-back"
                                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                                        <div class="owl-prop-img">
                                            <img src="{{ asset('images/Not_available.jpg') }}"
                                                 alt="" class="img-responsive" >
                                        </div>
                                           
                                    </div>
                          </div>
                          @else
                              @if($loop->count  >= 2)
                              @elseif($loop->count == 1 )
                              <div class="col-md-4">
                                <div class="owl-prop-item" > 

                                      <div class="owl-prop-back"
                                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                                        <div class="owl-prop-img">
                                            <img src="{{ asset('images/Not_available.jpg') }}"
                                                 alt="" class="img-responsive">
                                        </div>
                                           
                                    </div>
                              </div>
                              @endif
                              <div class="col-md-4">
                              <div class="owl-prop-item" > 

                                      <div class="owl-prop-back"
                                             style="background-image:url({{ $uri.$datas->filePreview }});"></div>
                                        <div class="owl-prop-img">
                                            <img src="{{ $uri.$datas->filePreview }}"
                                                 alt="" class="img-responsive">
                                        </div>
                                           
                                    </div>
                              </div>
                              @if($loop->count >= 3)
                                
                              @elseif($loop->count == 1)
                              <div class="col-md-4">
                              <div class="owl-prop-item" > 

                                      <div class="owl-prop-back"
                                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                                        <div class="owl-prop-img">
                                            <img src="{{ asset('images/Not_available.jpg') }}"
                                                 alt="" class="img-responsive">
                                        </div>
                                           
                                    </div>
                              </div>
                              @elseif($loop->count == 2)
                              <div class="col-md-4">
                              <div class="owl-prop-item" > 

                                      <div class="owl-prop-back"
                                             style="background-image:url({{ asset('images/Not_available.jpg') }});"></div>
                                        <div class="owl-prop-img">
                                            <img src="{{ asset('images/Not_available.jpg') }}"
                                                 alt="" class="img-responsive">
                                        </div>
                                           
                                    </div>
                              </div>
                              @endif


                          @endif
                        
                      @endforeach
                
                @endif
              </div>
              <!-- End Owl carousel -->
            </div>
          <div id="tab22" class="tab">
      
              <div id="map4"></div>
            
          </div>
            <div id="tab3" class="tab">
            </div>
          </div>
          <ul class="tab-links col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
             <li class="active col-xs-6"><a href="#tab1"><img src="{{ asset('assets/img/camera-black.png') }}"
                                                                     alt=""/>Photo</a></li>
                    <li class="col-xs-6"><a href="#tab22" class="map4"><img src="{{ asset('assets/img/map.png') }}"
                                                                       alt=""/>Map</a>
                    </li>
          </ul>
        </div>
      </div>
        

        <div class="wide-2">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('properties') }}">Property</a></li>
                    <li class="active">{{ $body->data->listTitle }}</li>
                     <h6 class="">Listing ID : <a href="">#{{ $body->data->id }}</a></h6>
                </ol>
            </div>
            <div class="container">
                <div class="row">
                    <aside class="pr-summary col-md-4 col-xs-12">
                        <form action="#">
                            <div class="">
                            @if($body->data->links->listListingCategoryId == null)
                            <div class="col-md-6 col-xs-6" style="padding-left: 0px;">
                              
                            </div>
                            @else
                                <div class="col-md-6 col-xs-6" style="padding-left: 0px;">
                                @if($body->data->links->listListingCategoryId === '1')  <label class="badge badge-sell"> For Sale </label> @elseif($body->data->links->listListingCategoryId === "2")  <label class="badge badge-rent"> For Rent </label> @endif
                                </div>
                            @endif
                                <div class="col-md-6" style="padding-right: 0px;">
                                <div class="price-property" style="text-align: right;font-family: 'Montserrat', sans-serif;"><?php
                                    $rp = $body->data->listListingPrice;
                                    $rp = (0 + str_replace(",", "", $rp));

                                    if ($rp >= 1000000000) {
                                        $rp = round(($rp / 1000000000), 3);
                                        echo 'Rp.' . ' ' . number_format($rp) . ' M';
                                    } else {
                                        $rp = round(($rp / 1000000), 0);
                                        echo 'Rp.' . ' ' . number_format($rp) . ' Jt';
                                    }
                                    ?></div>
                            <!--   @if($body->linked->listListingCategoryId != null)
                                @if($body->linked->listListingCategoryId->lsclName == 'Rent')
                                    <label class="badge badge-rent">{{$body->linked->listListingCategoryId->lsclName}}</label>

                                @elseif($body->linked->listListingCategoryId->lsclName == 'Sale')
                                    <label class="badge badge-sell">{{$body->linked->listListingCategoryId->lsclName}}</label>

                                @endif


                                @if($body->linked->listListingCategoryId->lsclName == 'Sewa')
                                    <label class="badge badge-rent">{{$body->linked->listListingCategoryId->lsclName}}</label>

                                @elseif($body->linked->listListingCategoryId->lsclName == 'Jual')
                                    <label class="badge badge-sell">{{$body->linked->listListingCategoryId->lsclName}}</label>

                                @endif
                              @else
                              @endif -->
                            </div>
                          </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-6 col-xs-6 cat-img">
                                            <img src="{{ asset('/assets/img/bedroom.png') }}" alt="">
                                            @if(is_null($body->data->listBedroom))
                                                <p> - Bedrooms</p>
                                            @else
                                                <p>{{ $body->data->listBedroom }} Bedrooms</p>
                                            @endif
                                        </div>
                                        <div class="col-lg-7 col-md-6 col-xs-6 cat-img cat-img">
                                            <img src="{{ asset('/assets/img/bathroom.png') }}" alt="">
                                            @if(is_null($body->data->listBathroom))
                                                <p> - Bathrooms</p>
                                            @else
                                                <p>{{ $body->data->listBathroom }} Bathroom</p>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-5 col-md-6 col-xs-6 cat-img">
                                                    <img src="{{ asset('/assets/img/map1.png') }}" alt="">
                                                    @if(is_null($body->data->listBuildingSize))
                                                        <p class="info_line">- m<span class="rank">2</span></p>
                                                    @else
                                                        <p class="info_line">{{$body->data->listBuildingSize}} m<span
                                                                    class="rank">2</span></p>
                                                    @endif
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-xs-6 cat-img">
                                                    <img src="{{ asset('/assets/img/square.png') }}" alt="">
                                                    @if(is_null($body->data->listBuildingSize))
                                                        <p class="info_line">- m<span class="rank">2</span></p>
                                                    @else
                                                        <p class="info_line">{{$body->data->listLandSize}} m<span
                                                                    class="rank">2</span></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="col-lg-5 col-md-6 col-xs-6 line"></div>
                                                    <div class="col-lg-5 col-md-6 col-xs-6 line"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="full-width">
                                </div>
                            </div>
                            <div class="picker-block col-md-12 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class=" col-xs-12" style="padding-left:0;">
                                    @if($membership->linked->mmbsFile != null)
                                        <div class="circle">
                                            <img src="{{ $membership->linked->mmbsFile->filePreview }}" style="background-size:cover;"
                                                 alt="">
                                        </div>
                                    @else
                                        <div class="circle">
                                            <img src="{{ asset('images/default.jpg') }}" style="background-size:cover;"
                                                 alt="">
                                        </div>
                                    @endif
                                    
                                        <div class="team-info">
                                        @if($body->linked->listMmbsId == null)
                                          <h3 id="agent-name">No Name</h3>
                                        @else
                                            <h3 id="  ">{{$body->linked->listMmbsId->mmbsFirstName}}</h3>
                                        @endif
                                        @if($body->linked->listOfficeId == null)
                                            <p class="team-color" style="color: #428bca;font-weight: 600; padding-left:0px">
                                              <a href="#">
                                                <span class="remax-red">RE<span class="remax-blue">/</span>MAX
                                                No Office
                                              </a>
                                            </p>
                                        @else
                                            <p class="team-color" style="color: #428bca;font-weight: 600; padding-left:0px">
                                                <!-- {{--{{ var_dump($body->linked->listOfficeId->frofFileId) }}--}} -->

                                                <a href="{{ route ('sub_route', ['domain' => strtolower(str_replace(' ', '', $body->linked->listOfficeId->frofOfficeName)), 'uri' => Request::path()]) }}" target="_blank">
                                                <span class="remax-red">RE<span class="remax-blue">/</span>MAX
                                                </span>
                                                {{ $body->linked->listOfficeId->frofOfficeName}}</a>
                                                {{--{{$body->linked->listFranchiseId}}--}}
                                            </p>
                                          @endif
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12" style="margin-bottom: 5px;">
                                <div class="ffs-bs">
                                    <button class="btn btn-primary" @click="onHandle">@{{ contact }}</button>
                                    <li class="list-group-item" v-show="flag"><i class="fa fa-envelope"></i>&nbsp;Email
                                        : {{ $membership->data->mmbsEmail ? $membership->data->mmbsEmail : '-'  }}</li>
                                    <li class="list-group-item" v-show="flag"><i class="fa fa-phone"></i>&nbsp;Phone
                                        Number
                                        : {{ $membership->data->mmbsCellPhone1 ? $membership->data->mmbsCellPhone1 : '-' }}
                                    </li>
                                </div>
                            </div>
                        </div>



                        <div class="share-news">
                            <ul>
                       
                                <li><a class="facebook customer share" href="http://www.facebook.com/sharer/sharer.php?u={{ route('property.show',$body->data->id) }}&title={{ $body->data->listTitle }}"><img src="{{ asset('assets/img/remax-fb.png') }}" alt=""></a></li>
                
                                <li><a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url={{ route('property.show',$body->data->id) }}&title={{ $body->data->listTitle }}"><img src="{{ asset('assets/img/remax-in.png') }}" alt=""></a></li>
                                <li><a class="twitter customer share" href="http://twitter.com/share?url={{ $body->data->listTitle }}+{{ route('property.show',$body->data->id) }}"><img src="{{ asset('assets/img/remax-tw.png') }}"></a></li>
                                <li><a class="google-plus customer share" href="https://plus.google.com/share?url={{ route('property.show',$body->data->id) }}"><img src="{{ asset('assets/img/remax-gp.png') }}"></a></li>
                                <li><a href="mailto:{{$membership->data->mmbsEmail}}?subject={{ $body->data->listTitle }}&amp;body={{ $body->data->listDescription }}. {{ route('property.show', $body->data->listUrl)}}"><img src="{{ asset('assets/img/remax-mail.png') }}" alt=""></a></li>
                                <li><a href="whatsapp://send?text={{ route('property.show',$body->data->listUrl) }}"><img src="{{ asset('assets/img/WhatsApp_Logo_1.png')}}"></a></li>
                            </ul>
                        </div>

                    </aside>
                    <div class="pr-info col-md-8 col-xs-12">
                       
                        <h2 style="text-align:left;">{{ $body->data->listTitle }}</h2>
                        @if($body->linked->listCityId == null)
                        <h5 class="team-color"><span class="fa fa-map-marker"></span> - </h5>
                        @else
                        <h5 class="team-color"> <span class="fa fa-map-marker"></span> {{ $body->linked->listCityId->mctyDescription }}
                            , {{ $body->data->listStreetName }}</h5>
                        @endif
                        <p>{{ $body->data->listDescription }}</p>
                    </div>

                    <div class="pr-info col-md-8 col-xs-12">
                        <h3>Property Features:</h3>
                        <section class="">
                            <ul class="submit-features" >


                                @if($body->data->links->listFacility == null)
                                    <span> coming soon </span>
                                @else     
                                  
                                    @foreach($body->data->links->listFacility as $value)
                                      @foreach($body->linked->listFacility as $data)
                                        @if(@$value->{$data->id})
                                              <li class="col-md-4 col-xs-4" style="padding-left:0px;">
                                                  <div>{{ @$value->{$data->id} }} {{ $data->fctlName }}</div>
                                              </li>
                                        @endif
                                      @endforeach            
                                    @endforeach

                                @endif

                            </ul>
                        </section>
                        </div>
                        <br />
    <div class="pr-info col-md-8 col-xs-12">  
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default fadeInDown">
      <div class="panel-heading" role="tab" id="heading">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="">
            <i class="more-less glyphicon glyphicon-plus"></i>
            Property Detail Description Feature
            <br />
             <small>This is small text</small>
          </a>
        </h4>
      </div>
      <div id="collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
        <div class="panel-body">
  <div class="media">
    <div class="media-left media-middle">

    </div>
    <div class="media-body">
<div class="section3">
        <div class="col-md-4 col-sm-4 col-xs-12 mob_mb20">
            <div class="section3_heading">
                <h2>Price</h2>
            </div>

            <div class="section3_body">
                <ul>
                    <li>Awesome</li>
                    <li>Cool</li>
                    <li>Beautiful</li>
                </ul>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 mob_mb20">
            <div class="section3_heading">
                <h2>Env</h2>
            </div>

            <div class="section3_body">
                <ul>
                    <li>Awesome</li>
                    <li>Cool</li>
                    <li>Beautiful</li>
                </ul>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 mob_mb20">
            <div class="section3_heading">
                <h2>Access</h2>
            </div>

            <div class="section3_body">
                 <ul>
                    <li>Awesome</li>
                    <li>Cool</li>
                    <li>Beautiful</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
      <footer class="text-right">
              </footer>
    </div>
  </div>
        </div>
      </div>
    </div>
</div>


                    </div>
                </div>
            </div>
        </div>
        <div class="wide_container_3">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 center">
                        <h3 style="text-align: center;color:#3A7DE3;"> Ada pertanyaan tentang property ini ?</h3>

                        <h2 style="text-align: center;color:#DE1922;"><strong> Saya siap membantu </strong></h2>

                        <br>

                    <form @submit.prevent="validateBeforeSubmit" method="post">
                        <div class="wide-inner row">

                            <div class="col-md-6" style="padding-left: 0;">
                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('name')}">
                                <input v-validate="'required|alpha'" :class="{'input': true, 'is-danger': errors.has('name')}" style="text-transform: capitalize;" v-model="inquiry.name" name="name" class="form-control" type="text"
                                       placeholder="name">
                                    <span v-show="errors.has('name')" class="text-danger">@{{ errors.first('name') }}></span>
                                </div>

                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('email')}">
                                    <input v-validate="'required|email'" style="text-transform: capitalize;" class="form-control" v-model="inquiry.email" name="email" type="text" placeholder="Email">
                                    <span v-show="errors.has('email')" class="text-danger">@{{ errors.first('email') }}</span>
                                </div>

                                <div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('phone')}">
                                    <input v-validate="'required|numeric'" style="text-transform: capitalize;" class="form-control" v-model="inquiry.phone" name="phone" type="text" placeholder="0857xxxxx">
                                    <span v-show="errors.has('phone')" class="text-danger">@{{ errors.first('phone') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6"
                                 style="padding-right:15px;padding-left: 0;width: 50%;float: right;">
                                    <textarea v-model="inquiry.message" name="message" class="form-control"
                                              style="height:130px;width: 100%;margin-bottom: 10px;text-transform: capitalize;"> </textarea>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-left:0">
                                <button class="btn btn-primary btn-block" @click="postInquiry">Send Message</button>
                            </div>
                          
                        </div>
                </form>

                    </div>
                   



                    <div class="col-md-4 col-xs-12 some-prp" style="height: 320px;overflow-y: scroll;">
                        <h4 style="color: black">Listing kantor <span class="remax-red">RE<span
                                        class="remax-blue">/</span>MAX</span> {{ $body->linked->listOfficeId->frofOfficeName }}
                        </h4>
                        <hr>
                        @foreach($office->data as $off)
                            
                            <div class="property-block-small">
                                <a href="{{$off->listUrl}}" target="_blank">
                           
                               @if($off-> links->listFile == null)

                                 <div class="property-image-small"
                                                             style="background-image: url({{ asset('images/Not_available.jpg') }}) "></div>

                               @else
                                    @foreach($office->linked->listFile as $file)
                                        @if($off->links->listFile[0] == $file->id)

                                          <div class="property-image-small"
                                                 style="background-image: url({{ $uri.$file->filePreview }}) "></div>   
                                       
                                        @endif
                                    @endforeach
                               
                                @endif
                                           
                                                     
                                 
                                    <h3>{{ \Illuminate\Support\Str::words($off->listTitle, 4, '...') }} @if($off->links->listListingCategoryId === '1') <span class="label label-danger pull-right"> For Sale </span> @elseif($off->links->listListingCategoryId === "2") <span class="label label-primary pull-right"> For Rent </span> @endif</h3>

                                    @if($off->listStreetName == null)
                                    <p class="team-color">-</p>
                                    @else
                                    <p class="team-color">{!! \Illuminate\Support\Str::words($off->listStreetName, 3, '...') !!}</p>
                                    @endif
                                    <h4 class="price-property" style="color:red;">
                                        <?php
                                        $rp = $off->listListingPrice;
                                        $rp = (0 + str_replace(",", "", $rp));
                                        if ($rp >= 1000000000) {
                                            $rp = round(($rp / 1000000000), 3);
                                            echo 'Rp.' . ' ' . number_format($rp) . ' M';
                                        } else {
                                            $rp = round(($rp / 1000000), 0);
                                            echo 'Rp.' . ' ' . number_format($rp) . ' Jt';
                                        }
                                        ?>
                                    </h4>
                                </a>
                                <hr>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Page Content -->

    <!-- Start Footer -->
    <div id="footer" class="cihuy">@include('layout.footer')</div>

    <!-- End Footer -->
</div>


<!-- Use this code below only if you are using google street view -->
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initialize&libraries=places"></script> -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBt6Pwo89NTFbXWEtrcEsL14M6af-gSjfQ&libraries=places"></script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset('assets/js/vee-validate/dist/vee-validate.min.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
{{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>--}}
<script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/markerwithlabel_packed.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.raty.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/classie.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/ie.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/share-popup.js') }}"></script>

<![endif]-->

{{--<script>--}}
{{--FB.ui({--}}
{{--method: 'share',--}}
{{--mobile_iframe: true,--}}
{{--href: 'digikomdev.com/remaxmockup/search'--}}
{{--}, function(response){});--}}
{{--</script>--}}

<script>
//    $(document).ready(function () {
//        $('.js-prop-carousel').owlCarousel({
//            nav: true,
//            navText: [
//                "<i class='icon-chevron-left icon-white'><</i>",
//                "<i class='icon-chevron-right icon-white'>></i>"
//            ],
//            slideSpeed: 300,
//            paginationSpeed: 400,
//            singleItem: true,
//            pagination: false,
//            items: 1,
//            loop: true,
//            rewindSpeed: 500
//        });
//
//    });

    $(".js-prop-carousel").owlCarousel({
        items : 3,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1024:{
                items:3
            }
        },
        pagination: true,
        nav: true,
        slideSpeed: 700,
        itemsDesktop: [1024,3],
        itemsDesktop: [480,1],
        loop:true,
        navText: [
            "<i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ]
    });
</script>


<script>
    Vue.config.devtools = true;
    Vue.use(VeeValidate);
    var propertyDetail = new Vue({
        el: '#page-property-content',
        data: {
            flag: false,
            flgEmail: false,
            email: '',
            name: '',
            phone: '',
            inquiry: {
                name: '',
                phone: '',
                status: '',
                email: '',
                message: ''
            },
            test: [],
            uriFile: 'https://www.remax.co.id/prodigy/papi/listing/',

            loading: false,
            idListing: "{{ $body->data->id }}",
        },

        created: function(){
          this.getHandle();
        },
        methods: {

            getHandle: function() {
                var vm = this;
                axios.get('https://www.remax.co.id/prodigy/papi/listing/crud/85/links/listingfile/115')
                // axios.get(vm.uriFile + 'crud/' + {{ $body->data->id }} + '/links/listingfile/' + {{ $body->data->links->listFile[0]}})
                    .then(function(response){
                       
                        vm.test = JSON.parse(response.data.status);

                    })


            },

            onHandle: function () {
                this.flag = !this.flag;
            },
            validateBeforeSubmit() {
                this.$validator.validateAll()
                    .then(function() {
                        alert('success send message');
                        location.reload();
                    })
                    .catch(function() {
                        alert('form must be fulfilled and cannot be empty');
                    })
            },
            postInquiry: function () {
                var vm = this;
               axios.post('https://www.remax.co.id/prodigy/papi/inquirylisting/crud',
                        {
                            "inagCustomerName": vm.inquiry.name,
                            "inagCustomerEmail": vm.inquiry.email,
                            "inagCustomerPhone": vm.inquiry.phone,
                            "inagCustomerBody:": vm.inquiry.message,
                            "links": {
                                "inliListing": 1

                            }
                        })
                        .then(function (response) {
                            vm.loading = true;
                            console.log(response)
                        })
                        .catch(function (error) {
                            vm.loading = false;
                            console.log(error)
                        })

            }

        },

        computed: {
            missingName: function() {
                return this.name === '';
            },

            phoneBtn: function () {
                if (this.flag == false) {
                    return 'Contact Agent';
                }
            }
            ,

            contact: function () {
                if (this.flag == false) {
                    return 'Show Contact';
                } else {
                    return 'Hide Contact';
                }
            },
            emailBtn: function () {
                if (this.flag == false) {
                    return 'Contact Agent';
                }
            }
        }
    });
</script>
<script>

     var cor = <?php 
     if($body->data->listCoordinat == ''){
       echo '{ coordinate: {
              latitude : -6.1751,
              longitude : 106.8650
       }
     }';
     }else{
      echo $body->data->listCoordinat;
     }
     ?> ;

            function initialize() {
                // console.log(this.cor)
                var vm = this;
                console.log(vm)
                var latitude = this.cor.coordinate.latitude;
                var longitude = this.cor.coordinate.longitude;
             
                var latlng = {lat: latitude, lng: longitude};
                var mapOptions = {
                    center: latlng,
                    zoom: 15  // default 15
                };

                this.map = new google.maps.Map(document.getElementById('map4'), {
                    zoom: 12,
                    center: {lat: latitude, lng: longitude},
                    // scrollwheel : false,
                    // disableDefaultUI: true,
                });

                var icon = {
                    url: '../images/new-map-marker.png',
                    scaledSize: new google.maps.Size(50, 50),
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                }

                var marker = new MarkerWithLabel({
                    position: latlng,
                    map: map,
                    // icon: {
                    //     scaledSize: new google.maps.Size(50, 50),
                    //     origin: new google.maps.Point(0, 0),
                    //     anchor: new google.maps.Point(0, 0)
                    // },
                    icon: icon
                    // labelContent: '<div class="marker-loaded"><div class="map-marker"><img src="{{ asset('images/map_marker.png') }}" height="100" width="100" alt="" /></div></div>',
                    // labelClass: "marker-style"
                });

                var contentString =
                        '<h4 class="firstHeading">{{ $body->data->listTitle }}</h4>' +
                         ' @if($body->linked->listCityId == null ) <h6"> - , {{ $body->data->listStreetName }} </h6>@else <h6"> {{ $body->linked->listCityId->mctyDescription }}, {{ $body->data->listStreetName }} </h6>@endif';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

                //resize for opeening and to get center of map
                $('.map4').bind('click', function () {
                    google.maps.event.trigger(map4, 'resize');
                    map.panTo(marker.getPosition());
                });

            };

                
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
</body>
</html>