<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.slider.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />

    <title>Album</title>
</head>

<body class="page-search" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->

    <!-- Page Content -->

    <div class="">
         {{-- style="background: url({{ asset('images/GALLERY_HEADER.jpg') }});background-size:cover;width: 100%;"> --}}
         <img src="{{ asset('images/GALLERY_HEADER.jpg') }}" class="img-responsive" >
    </div>

    <div class="container-news" style="margin-bottom:20px;">

        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="">Album</li>
        </ol>

        <div id="page-content-search">
            <div class="container-g p40-tb">
                <div class="row m0">
                    @foreach($body->data as $data)
                        <div class="col-md-3" style="padding-left: 0px;margin-left: -10px">
                            <a href="{{ route('gallery.detail',$data->id) }}">
                                <div class="gallery-list" data-date="10 September 2016"
                                     data-title="{{$data->wbgyTitle }}">
                                    <div class="s-image">
                                        <div class="inner-image">
                                            @foreach($body->linked->wbgyFileId as $linked)

                                                @if($data->links->wbgyFileId == $linked->id)
                                                <div class="image js-lazyload"
                                                     data-original="{{ $uri.$linked->filePreview.'?size=600,300' }}"
                                                     style="background-color: #ddd;"></div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div><!-- end Page Content -->
    </div>

    <!-- Start Footer -->
    <div id="footer">@include('layout.footer')</div>

    <!-- End Footer -->
</div>

<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.dependClass-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/draggable-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jshashtable-2.1_src.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.numberformatter-1.2.3.js') }}"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>



<script>
        $('.selection').selectize({sortField: 'text'});

        $(".js-lazyload").each(function () {
            $(this).lazyload({
                effect: "fadeIn"
            });
        });
</script>

<!--[if gt IE 8]> -->
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>
<![endif]-->
</body>
</html>

