<!DOCTYPE html>
<style>
    .page-homepage body{
        height: 100%;
        overflow: hidden;
    }

    .advance-search-home{
        font-size: 17px;
        color: white;
        text-underline-color: red;
        cursor: pointer;
        float:right;
    }
</style>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css')}}" type="text/css">
    <title>Home</title>
</head>

<body class="page-homepage" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="menu-wht"> @include('layout.header')</div>
    <!-- End Header -->

    <!-- Page Content -->
    <!-- <div>
        <div class="home_1">
            <div class="header_title">
                {{--<img src="{{ asset('images/remax_logo.png') }}" alt="" style="height: 80px;">--}}
                {{--<h1 style="margin-top: 0;text-align: center;"> RE/MAX Is There for You</h1>--}}
            </div>
            <form id="form-submit" class="form-submit" action="url(">
                <div class="search">
                    <div class="selector col-md-2  col-sm-2">
                        <select class="selection" id="rent-sale">
                            <option>Buy</option>
                            <option>Rent</option>
                        </select>
                    </div>

                    <div id="email-label" class="col-md-7 col-sm-7">
                        <i class="fa fa-location-arrow"></i>
                        <input type="text" id="search_field" class="form-control"
                               placeholder="Where do you like to live?">
                    </div>
                    <span class="ffs-bs col-md-2 col-sm-2"><button type="submit" class="btn btn-small btn-primary">Go
                        </button></span>
                </div>
            </form>
        </div>
    </div> -->

    <div>
        <!-- <div class="home-section" style="background-image:url('../../images/backgroundhome.jpg');"> ini kalau mau backgroundnya ganti2, tapi di css di ilangin backgroundimage nya-->
        <div class="home-section">
            <div class="home-center">
                <object class="master-logo" type="image/svg+xml"></object>
                <div class="tabs-wrap">
                    <ul class="row m0">
                        <li><a href="#buyForm" class="buy active">BUY</a></li>
                        <li><a href="#rentForm" class="rent">RENT</a></li>
                        <li class="pull-right"><h4 class="advance-search-home">Advance Search <i class="fa fa-sort-down"></i></h4></li>
                    </ul>

                </div>
                <div class="search-wrap">
                    <form action="{{ route('search') }}" id="buyForm">
                        <div class="row m0">
                            <input type="text" placeholder="Buy a House" class="form-control pull-left" name="buySearch">
                            <button type="submit" class="btn btn-small btn-primary pull-left">Go</button>
                        </div>
                    </form>
                    <form action="{{ url('search') }}" id="rentForm" class="hideit">
                        <div class="row m0">
                            <input type="text" placeholder="Rent a House" class="form-control pull-left" name="rentSearch">
                            <button type="submit" class="btn btn-small btn-primary pull-left">Go</button>
                        </div>
                    </form>
                </div>
				<div class="advSearch-wrap hideit">
                    <!-- <div class="advSearch-title">Advance Search</div> -->
                    <div class="row m0">
                        <div class="advSearch-input col-md-3">
                            <div class="title">Bedroom</div>
                            <select name="" id="">
                                <option value="1">Item 01</option>
                                <option value="2">Item 02</option>
                                <option value="3">Item 02</option>
                            </select>
                        </div>
                        <div class="advSearch-input col-md-3">
                            <div class="title">Area</div>
                            <select name="" id="">
                                <option value="1">Item 01</option>
                                <option value="2">Item 02</option>
                                <option value="3">Item 02</option>
                            </select>
                        </div>
                        <div class="advSearch-input col-md-3">
                            <div class="title">Min Price</div>
                            <select name="" id="">
                                <option value="1">Item 01</option>
                                <option value="2">Item 02</option>
                                <option value="3">Item 02</option>
                            </select>
                        </div>
                        <div class="advSearch-input col-md-3">
                            <div class="title">Max Price</div>
                            <select name="" id="">
                                <option value="1">Item 01</option>
                                <option value="2">Item 02</option>
                                <option value="3">Item 02</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
		
        </div>
    </div>
</div>



<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>
<!--[endif]-->
<!-- Begin - Custom Javascript -->
<script>
    $(".tabs-wrap ul li a").on("click", function(){
        var href = $(this).attr("href");
        $(".tabs-wrap ul li a").removeClass("active");
        $(this).addClass("active");

        $(".search-wrap form").addClass("hideit");
        $(href).removeClass("hideit");
    });
</script>
<!-- End - Custom Javascript -->


</body>
</html>