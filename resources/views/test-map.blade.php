<html>
<head>
    <title></title>
</head>

<style>
    #map{
        height: 600px;
        width:600px;
    }
</style>

<body>

<div id="root">
    <gmap-map style="width: 100%; height: 100%; position: absolute; left:0; top:0"
              :center="{lat: 1.38, lng: 103.8}"
              :zoom="12"
    >

    </gmap-map>
</div>


</body>

<script src="https://unpkg.com/vue@2.2.2"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.16.4/lodash.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{ asset('vue2-google-maps/dist/vue-google-maps.js') }}"></script>

<script>

    Vue.use(VueGoogleMaps, {
        load: {
            key: 'AIzaSyBt6Pwo89NTFbXWEtrcEsL14M6af-gSjfQ',
            v: '3.26'
            // libraries: 'places', //// If you need to use place input
        }
    });

    new Vue({
        el: '#root'
    });

</script>


</html>