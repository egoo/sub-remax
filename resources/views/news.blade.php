<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{asset('assets/fonts/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/customsas.css') }}" type="text/css">
    <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />
    <style>
    #media-accordion  .panel-heading{
        margin:0;
    }
  .image-news .caption-news {
      vertical-align: middle;
      text-align: center;
      top: 50%;
  }

  .caption-news span.border {
      background-color: #E21A22;
      opacity: 0.7;
      color: white;
      padding: 18px;
      letter-spacing: 5px;
      font-size: 13px;
      border: 2px;
      border-radius: 10px
  }

  .panel-group .panel {
    border-radius: 0;
    box-shadow: none;
    border-color: #EEEEEE;
  }

  .panel-default > .panel-heading {
    padding: 0;
    border-radius: 0;
    color: #212121;
    background-color: #FAFAFA;
    border-color: #EEEEEE;
  }

  .panel-title {
    font-size: 14px;
  }

  .panel-title > a {
    display: block;
    padding: 30px;
    text-decoration: none;
  }

  .more-less {
    float: right;
    color: #212121;
  }

  .panel-default > .panel-heading + .panel-collapse > .panel-body {
    border-top-color: #EEEEEE;
  }

    .fb-page, .fb-page:before, .fb-page:after {
    border: 1px solid #ccc;
  }

  .fb-page:before, .fb-page:after {
    content: "";
    position: absolute;
    bottom: -3px;
    left: 2px;
    right: 2px;
    height: 1px;
    border-top: none
  }

  .fb-page:after {
    left: 4px;
    right: 4px;
    bottom: -5px;
    box-shadow: 0 0 2px #ccc
  }
</style>

    <title>News</title>

</head>

<body class="page-news">

<div id="fb-root"></div>

<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=293151397519359";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<!-- Preloader -->
<div id="page-preloader">s
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->

    <!-- Page Content -->
    <div id="page-content" class="blog-styles">
        <div class="" >
             {{-- style="background-image: url({{ asset('images/backgroundPage/news.jpg') }})">
 --}}   <img src="{{ asset('images/backgroundPage/news1.jpg') }}" class="img-responsive">
            <div class="caption" style="top: 30%;">
                <span class="border hidden-xs" > RE/MAX News </span>
            </div>

        </div>

        <div class="container-article">

            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="">News</li>
            </ol>

            <div id="main" class="row">
                <div class="content-news row" style="margin-bottom: 40px;">


              <div class="grid-item">

          @foreach($body->data as $data)
         
              <article class="blog-post type-post format-image">
                   <a href="{{ url("news/$data->id") }}" style="color:#000">
                    <div class="header-text">
                       <h3>{{ $data->wbnlTitle }}</h3>
                    </div>
                    <figure class="meta">
                      <div class="post-date">
                        <p  class="link-icon" style="color:#C0C0C0;"><i class="fa fa-calendar"></i> {{ $data->wbnlCreatedTime }}</p>
                      </div>
                      <div class="tags pull-right">
                        <p class="tag article" style="color:#C0C0C0;">{{ $data->wbnlCreatedUserId }}</p>
                      </div>
                    </figure>
                  
                   @if($data->links->wbneFileId != null)
                                @foreach($body->linked->wbneFileId as $linked)

                                    @if($data->links->wbneFileId == $linked->id)
                                         
                                          <div style="float: left" class="col-md-4 col-xs-4">
                                            <img src="{{ $uri.$linked->filePreview }}" class="img-rounded img-responsive">
                                          </div>
                                          {{-- <div class="post-thumbnail" style="background: url(https://www.remax.co.id/prodigy/papi/Webnews/14/links/wbneFileId/51) center;height:280px;"></div> --}}
                                                                            @endif

                                @endforeach

                      @else

                            
                                          <div style="float: left" class="col-md-4 col-xs-4" >
                                            <img src="{{ asset('images/Not_available.png') }}" class="img-rounded img-responsive">
                                          </div>
                                          {{-- <div class="post-thumbnail" style="background: url(https://www.remax.co.id/prodigy/papi/Webnews/14/links/wbneFileId/51) center;height:280px;"></div> --}}
                                          @endif
                    </a>
                                  
                    <a href="{{ url("news/$data->id") }}" style="color:#000">
                    <p>
                    @if($data->id != $data->id = '25')
                    {!!  \Illuminate\Support\Str::words($data->wbnlContent,100, '...') !!}
                   @else
                    {!! $data->wbnlContent !!}
                    @endif
                    </p>

                    <div class="button-continue" style="color:  #1E90FF;">
                     Read Article
                    </div>
                    </a>
                  </article>
                  
        @endforeach

                 


                  <nav class="site-navigation paging-navigation navbar">
                        <div class="nav-previous"><a href="">PREV PAGE</a></div>
                        <ul class="pagination pagination-lg">
                          <li><a href="#">1</a></li>
                          <li><span class="active">2</span></li>
                          <li><a href="#">3</a></li>
                          <li><span class="nav-dots">...</span></li>
                          <li><a href="#">5</a></li>
                        </ul>
                        <div class="nav-next"><a href="#">NEXT PAGE</a></div>
                      </nav>
</div>




                 {{--   @foreach($body->data as $data)
                        <a href="{{ route('news.detail',$data->id) }}">
                            <article class="article-news">
                                <div class="article-inner">
                                    @if($data->links->wbneFileId != null)
                                        @foreach($body->linked->wbneFileId as $linked)

                                            @if($data->links->wbneFileId == $linked->id)
                                                <div class="article-image">
                                                    <div class="image" style="background-image: url({{ $uri.$linked->filePreview }})"></div>
                                                </div>
                                            @endif

                                        @endforeach

                                    @else
                                        <div class="article-image">
                                            <div class="image"
                                                 style="background-image: url({{ asset('images/Not_available.png') }})"></div>
                                        </div>
                                    @endif

                                    <div class="article-content">
                                        <div class="article-wrapper">
                                            <div>
                                                <span class="news-date">{{ Carbon\Carbon::parse($data->wbnlCreatedTime)->format('D')  }}
                                                    , {{ $data->wbnlCreatedTime }}</span>
                                            </div>
                                            <a href="{{ route('news.detail',$data->id) }}"><h2
                                                        class="article-title">{{ $data->wbnlTitle }}</h2></a>
                                        </div>
                                        <div>
                                            <span class="news-created">{{ $data->wbnlCreatedUserId }}</span>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </a>
                    @endforeach --}}

                </div>
                <div class="sidebar-facebook">
                    <aside id="sidebar-news">
                        <div class="block-news">

                            <div class="fb-page" data-href="https://www.facebook.com/remaxindo/" data-tabs="timeline"
                                 data-width="280" data-small-header="true" data-adapt-container-width="true"
                                 data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/remaxindo/" class="fb-xfbml-parse-ignore"><a
                                            href="https://www.facebook.com/remaxindo/">REMAX Indonesia</a></blockquote>
                            </div>

                        </div>
                    </aside>

                </div>
            </div>

        </div>
    </div>

    <!-- Start Footer -->
    <div id="footer">@include('layout.footer')</div>
    <!-- End Footer -->
</div><!-- Wrapper -->

<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<!--[if gt IE 8]> -->
<script type="text/javascript" src="{{ asset('assets/js/custom.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/readmore.min.js') }}"></script>
<!--[endif]-->
<!--
<script>
    $('.article-news').readmore({
        collapsedHeight: 300
    });
</script> -->
{{--
<script type="text/javascript">
            $(document).ready(function () {
                $('a[data-toggle="collapse"]').click(function () {
                    $(this).find('span.toggle-icon').toggleClass('glyphicon-collapse-up glyphicon-collapse-down');
                })
            })
        </script> --}}

<script>

  function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
</body>

</html>