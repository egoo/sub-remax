<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/fonts/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.slider.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" type="text/css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.8/css/lg-fb-comment-box.min.css"
          type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.8/css/lg-transitions.min.css"
          type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.8/css/lightgallery.min.css"
          type="text/css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/justifiedGallery/3.6.3/css/justifiedGallery.min.css"
          type="text/css">
          <link rel='shortcut icon' type='image/x-icon' href='/assets/img/favicon.ico' />

    <title>Gallery</title>
</head>

<body class="page-search" id="page-top">
<!-- Preloader -->
<div id="page-preloader">
    <div class="loader-ring"></div>
    <div class="loader-ring2"></div>
</div>
<!-- End Preloader -->

<!-- Wrapper -->
<div class="wrapper">
    <!-- Start Header -->
    <div id="header" class="prop">@include('layout.header')</div>
    <!-- End Header -->

    <!-- Page Content -->
    <div id="page-content-search">
        <div class="text-center"
             style="background: url({{ asset('images/GALLERY_HEADER.jpg') }});background-size:cover;">
        </div>


        <div class="searchDet-title" style="margin-left: 4.8%">
            <h2 style="font-weight: bold;">{{ $body->linked->wbgaWbgyId[0]->wbgyTitle }}</h2>

        </div>
        <div class="container-news" style="margin-bottom:20px;">

            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('albums') }}">Album</a></li>
                <li>{{ $body->linked->wbgaWbgyId[0]->wbgyTitle }}</li>
            </ol>

        </div>
        <div class="row m0">
            <div id="js-lightgallery" class="row">

                @if ($body->linked->wbgaFileId == null)
                        @foreach($body->data as $data)
                        <a href="https://www.youtube.com/watch?v={{ $data->wbgaUrlYoutube }}" data-poster="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg">
                            <div class="photos-item">
                                <img class="lazy" data-original="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg" src="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg" />
                            </div>
                            </a>
                        @endforeach
                @else
                     @foreach($body->linked->wbgaFileId as $linked)
                    <a href="{{$uri.$linked->filePreview}}" data-pinterest-text="Pin it" data-tweet-text="Tweet it" data-facebook-text="Share It">
                        <div class="photos-item">
                            <img class="lazy" data-original="{{$uri.$linked->filePreview }}" src="{{$uri.$linked->filePreview }}">
                        </div>
                    </a>
                @endforeach
                @foreach($body->data as $data)
                            @if(null === $data->wbgaUrlYoutube)
                            @else
                                <a href="https://www.youtube.com/watch?v={{ $data->wbgaUrlYoutube }}" data-poster="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg">
                                    <div class="photos-item">
                                        <img class="lazy" data-original="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg" src="http://img.youtube.com/vi/{{$data->wbgaUrlYoutube}}/maxresdefault.jpg" />
                                    </div>
                                </a>
                            @endif
                        @endforeach
                @endif

            </div>


        </div>
    </div><!-- end Page Content -->


    <!-- Start Footer -->
    <div id="footer">@include('layout.footer')</div>

    <!-- End Footer -->
</div>

<!-- Modal login, register, custom gallery -->
<div id="login-modal-open"></div>
<div id="register-modal-open"></div>


<!-- End Modal login, register, custom gallery -->
<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.placeholder.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/tmpl.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.dependClass-0.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jshashtable-2.1_src.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.numberformatter-1.2.3.js') }}"></script>

<script type="text/javascript"
        src="https://cdn.jsdelivr.net/g/lightgallery,lg-autoplay,lg-fullscreen,lg-hash,lg-pager,lg-share,lg-thumbnail,lg-video,lg-zoom"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/justifiedGallery/3.6.3/js/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.lazyload.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/js/ie.js')}}"></script>

<script>

    $("img.lazy").lazyload({
        effect: "fadeIn"
    });

    $('.selection').selectize({sortField: 'text'});

    $('#js-lightgallery').lightGallery({
        mode: 'lg-fade',
        thumbnail: true
    });

    $(".js-justifiedGal").justifiedGallery({
        rowHeight: 240
    });

</script>


</body>
</html>

