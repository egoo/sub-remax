
<div class="header-wrapper">
        <div class="header">
          <div class="container">
            <div class="header-inner">
              <div class="navigation-toggle toggle">
                <span></span>
                <span></span>
                <span></span>
              </div>
              <!-- /.header-toggle -->
              <div class="header-logo">
               
                  <img src="{{ asset('assets/img/logo_remax.jpg') }}" class="svg" alt="Home">
                
              </div>
              <!-- /.header-logo -->
              <div class="header-nav pull-right">
                <div class="primary-nav-wrapper">
                  <ul class="nav">
                    @if($header->data)
                        @foreach($header->data as $data)
                            <li class="nav-item " >
                              <a href="{{ $data->wbmnAPI }}" class="nav-link {{ Request::is($data->wbmnAPI) ? 'active' : '' }}">{{ $data->wbmlName }}</a>
                            </li>

                        @endforeach
                    @endif
                    @if($language->data)
                    @foreach($language->data as $lg)
                        <li class="nav-item" >
                            <a href="?language={{$lg->langCode }}" class="">
                                    @foreach($language->linked->langFileId as $linked)
                                        @if($lg->links->langFileId == $linked->id)
                                            <img style="width: 30px; height: 15px; margin: 22px 7px 0px 0px;" src="{{ 'https://www.remax.co.id/prodigy/papi/'.$linked->filePreview.'?size=30,32' }}" alt="" class="img-responsive">
                                        @endif
                                    @endforeach
                            </a>
                        </li>
                    @endforeach
                    @endif
            
                  </ul>
                </div>
                <!-- /.primary-nav-wrapper -->
              </div>
             
              <!-- /.header-toggle -->
             
              <!-- /.header-actions -->
            </div>
            <!-- /.header-inner -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.header -->
      </div>


