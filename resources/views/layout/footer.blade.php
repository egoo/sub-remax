<style>

    address {
        font-size: 12px;
    }

    address ul li {
        display: inline;
        list-style-type: none;
        color: white;
    }

</style>
{{-- <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Footer</title>
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}" type="text/css">
</head>
<body> --}}
<footer id="page-footer">
    <div class="inner">
        <section id="footer-main">
            <div class="container-news">
                <div class="row">
                    <div class="col-md-6 col-sm-6" style="padding-left: 2px;margin-left: -20px;">
                        <article class="contact-us">
                            <br><br>
                            <object class="comp-logo">
                            </object>
                            <address>

                                <ul>

                                        <li style="float: left;margin-right: 20px;margin-bottom: 4%;"><?php echo substr($footer->data->compAddress, 0,23) ?><br><?php echo substr($footer->data->compAddress, 23,27) ?><br><?php echo mb_strimwidth($footer->data->compAddress, 50, 51)?><br>
                                        <i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;<?php echo $footer->data->compPhone1 .
                                                    '<br>' . '<i class="fa fa-fax"></i>' . '&nbsp;&nbsp;' . $footer->data->compFax1 . '<br>' .
                                                    '<i class="fa fa-envelope"></i>' . '&nbsp;&nbsp;&nbsp;' . $footer->data->compEmail;?></li>
                                       <!--  <li style="float: left;"><i
                                                    class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;<?php echo $footer->data->compPhone1 .
                                                    '<br>' . '<i class="fa fa-fax"></i>' . '&nbsp;&nbsp;' . $footer->data->compFax1 . '<br>' .
                                                    '<i class="fa fa-envelope"></i>' . '&nbsp;&nbsp;&nbsp;' . $footer->data->compEmail;?>
                                        </li>
 -->
                                </ul>
                            </address>

                            <br><br><br>
                        </article>
                    </div>
                </div><!-- /.col-sm-3 -->
            </div><!-- /.row -->
        </section>
    </div><!-- /.container -->

    <section id="footer-thumbnails" class="footer-thumbnails"></section><!-- /#footer-thumbnails -->

</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99368247-1', 'auto');
  ga('send', 'pageview');

</script>

{{-- </body>
</html>

 --}}