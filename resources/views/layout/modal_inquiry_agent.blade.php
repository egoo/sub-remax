{{-- <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modal error report</title>
	<style>
		.form-group{
			margin-bottom: 0px;
		}
	</style>
</head>
<body> --}}
	<div id="modal-inquiry" class="modal" tabindex="-1">
		<div id="form-inquiry">
<div class="modal-dialog">
			<form class="modal-content" @submit.prevent="validateBeforeSubmit">
				<div class="modal-header text-center" style="height:150px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h2>Become a Proud RE/MAX AGENT</h2>
					<span>Unlimited Income Opportunities</span>
				</div>
				<div class="modal-body">
					<div class="form-group" style="margin-bottom: 0px">
						<div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('name')}">
							Name : <input v-validate="'required|alpha'" :class="{'input': true, 'is-danger': errors.has('name')}" style="text-transform: capitalize;" v-model="inquiry.name" type="text" name="name">
							<span v-show="errors.has('name')" class="text-danger">@{{ errors.first('name') }}></span>
						</div>
					</div>
					<div class="form-group">
						<div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('email')}">
							Email : <input v-validate="'required|email'" :class="{'input': true, 'is-danger': errors.has('email')}" style="text-transform: capitalize;" v-model="inquiry.email" type="text" name="email">
							<span v-show="errors.has('email')" class="text-danger">@{{ errors.first('email') }}></span>
						</div>
					</div>
					<div class="form-group">
						<div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('phone')}">
							Phone : <input v-validate="'required|numeric'" :class="{'input': true, 'is-danger': errors.has('phone')}" style="text-transform: capitalize;" v-model="inquiry.phone" type="text" name="phone">
							<span v-show="errors.has('phone')" class="text-danger">@{{ errors.first('phone') }}></span>
						</div>
					</div>

					<div class="form-group">
						<div :class="{ 'control': true , 'input': true, 'is-danger': errors.has('message')}">
							Message :
							<textarea v-validate="" :class="{'input:': true, 'is-danger': errors.has('message')}" v-model="inquiry.message" style="width: 100%; height: 150px; margin-top: 10px;text-transform: capitalize;" name="message"></textarea>
							<span v-show="errors.has('message')" class="text-danger">@{{ errors.first('message')}}</span>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="padding: 10px;margin-top: 0px;">
					<div class="form-group">
						<button id="error-submit" type="submit" class="btn btn-block" @click="postInquiry">Submit</button>
					</div>
				</div>
			</form>
		</div>
		</div>



	</div>
