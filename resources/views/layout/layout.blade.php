<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{ asset('assets/assets/libraries/slick/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/libraries/slick/slick-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/trackpad-scroll-emulator.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/chartist.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/jquery.raty.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/nouislider.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/explorer-red.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">
    <title>Remax</title>
  </head>
  <body class="">
   <div class="page-wrapper">
       @yield('header')
   
        @include('layout.header')

      
        
        @yield('content')

      

        @yield('footer')
         <div class="footer-wrapper">
        <div class="footer">
          <div class="footer-inner">
            <div class="container">
              <div class="row">
                <div class="col-lg-4">
                  <div class="widget">
                    <h3>
                      <img src="{{ asset('assets/assets/img/REMAX_mastrwordmark.png') }}" class="svg" alt="Home">
                    </h3>
                    <br>
                   
                </div>
              </div>
               </div>
               <div class="row">
              <div class="col-lg-4">
                    <p>
                      Remax Kantor 
                    </p>
                    <p>
                      081276473812 Ruko Syahidinandar 06
                    </p>
                    <p>
                      Palembang
                    </p>

                    <ul class="social">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google"></i></a></li>
                    </ul>
              </div>
               </div>
                <!-- /.col-* -->
               
                <!-- /.col-* -->
               
                <!-- /.col-* -->
             
              <!-- /.row -->
            </div>
            <!-- /.container -->
          </div>
          <!-- /.footer-inner -->
        </div>
        <!-- /.footer -->
      </div>  
      </div>
      

    <!-- /.side-overlay -->

    <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&key=AIzaSyDmXybAJzoPZ6hH-Jhv7QMCSGgQ6MY8WqY" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/chartist.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/google-map-richmarker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/google-map-infobox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/google-map-markerclusterer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/google-map.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.trackpad-scroll-emulator.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.inlinesvg.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.affix.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.scrollTo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/libraries/slick/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/nouislider.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/jquery.raty.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/wNumb.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/particles.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/explorer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/assets/js/explorer-map-search.js') }}"></script>
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script src="{{ asset('js/vue-filter.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuejs-paginator/2.0.0/vuejs-paginator.js"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/lodash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
    
   @yield('script')
  </body>
</html>